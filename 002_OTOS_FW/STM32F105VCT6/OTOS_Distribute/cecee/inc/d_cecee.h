#ifndef CECEE_H_
#define CECEE_H_

#include "cecee_comdef.h"
#include "d_cecee_util.h"
#include "../drv/drv_dcan.h"
#include "../drv/tsk_100ms.h"
#include "../drv/Drv_dconsol.h"
#include "../util/cbuffer.h"

#include <stdint.h>


extern DB_UPLOAD db_up;
//extern CBD_CTRL_CMD CCtrlCmd[12];
extern CBD_DATA CbData[12];

extern uint8_t ONE_SEC_FLAG;
extern uint8_t CAN_RCV_FLAG;
extern uint8_t ONE_SEC_CNT;
extern uint8_t TEN_MIL_FLAG;
extern uint8_t HUN_MIL_FLAG;
extern uint8_t TEN_MIL_CNT;
extern uint8_t TICK_100M_CNT;
extern uint8_t CAN_TX_FLAG;

extern uint32_t gAdcValue[4];//adc
extern QUEUE qUart1;
extern QUEUE qUart3;
extern QUEUE qUart4;

extern uint8_t th_irq_cnt[3];
extern uint16_t th_irq_buf[3][60];
extern uint16_t th_start_tick;
extern uint8_t th_start;
//extern uint8_t th_ready;
extern uint8_t DHT_READY_FLAG;

extern BD_CONTROL BdCtrl;

//extern uint8_t update_ready;
/*
//#define SOLENODE_TYPE 0 //0:SOLENOID 1:MOTER
//#define FW_VERSION 0x806A //0:SOLENOID 1:MOTER
typedef struct tag_upload
{
	BMS_VALUE bms_value;
	SPS_VALUE chg_value;
	SOLENOID_VALUE sol_value;
}CB_UPLOAD;

extern CB_UPLOAD gCBUP;


extern TIME_OUT_LED led_time_out;
extern UI_FND ui;
extern uint32_t gAdcValue[16];//adc

// For BMS
extern uint8_t BMS_RCV_FLAG;
extern uint8_t CONSOLE_RCV_FLAG;

extern uint8_t gConsole_serch_flag;
extern uint8_t gBms_serch_flag;

extern uint8_t gBms_try;
extern uint8_t gBms_add;
extern uint8_t bms_buf[64];
extern uint8_t console_buf[64];
extern uint8_t bmsAddBuf[16];
extern  uint8_t bms_data_cnt;
extern uint8_t bms_messge;
// For CAN1
extern uint32_t myboard_id;
extern uint8_t can1_rxbuf[8];
*/
#endif
