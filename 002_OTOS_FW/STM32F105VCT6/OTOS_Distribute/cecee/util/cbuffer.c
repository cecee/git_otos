/*
 * cbuffer.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "cbuffer.h"
#include "d_cecee.h"

/****************************************************************************
 *                                Defines
*****************************************************************************/

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/
QUEUE qUart1;
QUEUE qUart3;
QUEUE qUart4;

uint8_t qUart1_package=0;
uint8_t qUart1_buffer[128];
uint8_t qUart1_cnt=0;
uint8_t qUart1_sz=0xff;

/*****************************************************************************
*                           Static Variables
******************************************************************************/

/////////////////////////////////////////////
///Queue
////////////////////////////////////////////

void init_qUart1(void){
 // initQue(qUart3);
 // initQue(qUart4);  
  qUart1.front=0;
  qUart1.rear=0;

}

void init_qUart3(void){
 // initQue(qUart3);
 // initQue(qUart4);  
  qUart3.front=0;
  qUart3.rear=0;

}

void init_qUart4(void){
  qUart4.front=0;
  qUart4.rear=0;
}

void init_queue(void){
 // initQue(qUart3);
 // initQue(qUart4);  
  qUart1.front=0;
  qUart1.rear=0;
  qUart3.front=0;
  qUart3.rear=0;
  qUart4.front=0;
  qUart4.rear=0;
}
  
/*
void clear_qUart3(void){
	qUart3.front=qUart3.rear=0;
}
void clear_qUart4(void){
	qUart4.front=qUart4.rear=0;
}

*/
int put(QUEUE q, int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((q.rear + 1) % MAX_SIZE == q.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",q.rear, q.front);
        return -1;
    }

    q.queue[ q.rear] = k;
    q.rear = (q.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", q.rear);
    return k;
}



int put_qUart1(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart1.rear + 1) % MAX_SIZE == qUart1.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart1.rear, qUart1.front);
        return -1;
    }

    qUart1.queue[ qUart1.rear] = k;
    qUart1.rear = (qUart1.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart1.rear);
    return k;
}

int put_qUart3(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart3.rear + 1) % MAX_SIZE == qUart3.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart3.rear, qUart3.front);
        return -1;
    }

    qUart3.queue[ qUart3.rear] = k;
    qUart3.rear = (qUart3.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart4.rear);
    return k;
}

int put_qUart4(int k){
	// printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((qUart4.rear + 1) % MAX_SIZE == qUart4.front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",qUart4.rear, qUart4.front);
        return -1;
    }

    qUart4.queue[ qUart4.rear] = k;
    qUart4.rear = (qUart4.rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", qUart4.rear);
    return k;
}


int get_qUart1(void)
{
    int i=0;
     if (qUart1.front == qUart1.rear){
        return -1;
    }
    i = qUart1.queue[qUart1.front];
   qUart1.front = (qUart1.front+1) % MAX_SIZE;
    return i;
}

int get_qUart3(void)
{
    int i=0;
     if (qUart3.front == qUart3.rear){
        return -1;
    }
    i = qUart3.queue[qUart3.front];
   qUart3.front = (qUart3.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}

int get_qUart4(void)
{
    int i=0;
     if (qUart4.front == qUart4.rear){
        return -1;
    }
    i = qUart4.queue[qUart4.front];
   qUart4.front = (qUart4.front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", q.front);
    return i;
}

int GetSerialBuf_console(void)
{
	int  i,d;
	int end=0;
//	for(i=0;i<10;i++)
	for(;;)
	{
		d =get_qUart4();
		if(d==-1 )break;
			
		switch(d){
			case 0x6D://m
				Drv_dbd_view();
				break;
			case 0x71://q
				Drv_dbd_cinfo(0);
				break;	
 			case 0x77://w
				Drv_dbd_cinfo(1);
				break;       
		
		}	
   // if(d==0x6D)   Drv_dbd_view();
  		printf("GetSerialBuf  [%02X]\r\n", d);
   
		if(end) break;	
	}
  return 1;
}

int GetSerialBuf_fromM(void)
{
	int  i,d;
	int end=0;

	for(;;)
	{
		d =get_qUart1();
		if(d==-1 )break;
     printf("---GetSerialBuf[%x]\r\n",d);
    
		if((qUart1_package==0) && (d==0x21))
    {
      printf("---GetSerialBuf_fromM Find_header---\r\n");
			qUart1_package=1;
			qUart1_cnt=0;
 			memset(qUart1_buffer,0,sizeof(qUart1_buffer));
		}	
    
 		if(qUart1_package)
    {
			qUart1_buffer[qUart1_cnt++]=d;
      if( d==0x40)
      {
        qUart1_package=0;
       	printf("--GetSerialBuf_fromM FindTail qUart1_cnt[%d] sz[%d]--\r\n",qUart1_cnt, qUart1_sz);
        Drv_Control_IO(qUart1_buffer);
        
      //	memcpy(&BdCtrl, &qUart1_buffer, qUart1_sz );
      //	for(i=0;i<10;i++)	printf("GetSerialBuf_fromM [%d/%d] [%02X]\r\n",i,sz, qUart1_buffer[i]);
      }
      if(qUart1_cnt>15)
      {
        qUart1_package=0;
        printf("--rcv ovver countet --\r\n");
     
      }
      //if(end) break;	
    }
    
  }
  return 1;
}