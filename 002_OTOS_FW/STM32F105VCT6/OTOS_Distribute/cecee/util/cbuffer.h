/*
 * cbuffer.h
 *
 */ 


#ifndef CBUFFER_H_
#define CBUFFER_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/
#define MAX_SIZE 512 // max size of queue
typedef struct _queue   
{
	int queue[MAX_SIZE+1];
	int front;
	int rear;
}QUEUE;

/****************************************************************************
*                                Functions
*****************************************************************************/
void init_queue(void);
void init_qUart1(void);
void init_qUart3(void);
void init_qUart4(void);


int get_qUart1(void);
int put_qUart1(int k);

int get_qUart3(void);
int put_qUart3(int k);

int get_qUart4(void);
int put_qUart4(int k);
//void clear_queue(QUEUE q);
//int put(QUEUE q, int k);


int GetSerialBuf_console(void);
int GetSerialBuf_fromM(void);
#endif /* CBUFFER_H_ */
