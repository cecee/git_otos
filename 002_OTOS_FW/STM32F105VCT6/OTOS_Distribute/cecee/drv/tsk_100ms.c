//#include "main.h"
#include "stm32f1xx_hal.h"
#include "tsk_100ms.h"
#include "d_cecee.h"
#include "dac.h"


//uint8_t bms_frame=0;
// uint8_t fsz=0xff;
//extern STATUS_SPS status_sps;

void tsk_100ms_init(void)
{
  Hal_ADC_Init();
//  HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
  HAL_DAC_Start(&hdac, DAC_CHANNEL_2);
}

void tsk_100ms_process(void){
//  printf("gAdcValue [%04x][%04x][%04x]\r\n",(uint16_t)gAdcValue[0],(uint16_t)gAdcValue[1],(uint16_t)gAdcValue[2]);
  Drv_gpio_getport();
}




/*
adc0->gAdcValue[0]--V1_MONITOR
adc1->gAdcValue[1]--V2_MONITOR
adc2->gAdcValue[2]--C1_MONITOR
adc3->gAdcValue[3]--BMS_V12 DETECTOR
adc6->gAdcValue[4]--TEMPER From sps194
adc7->gAdcValue[5]--TEMPER From TMP36
adc8->gAdcValue[6]--RSEV From sps194
adc9->gAdcValue[7]--BMS_GND(current) DETECTOR
*/
/*
void tsk_100ms_process(void)
{
  static uint8_t seq=0;
   uint16_t ttt;
//	GetSerialBuf();
	seq++;
	seq%=2;
	if(seq==0)	Drv_bms_monitor();
  	Drv_solenoid_get_pos();
  	if(gCBUP.chg_value.b.batt_exist)  Drv_sps194_step();
  	else {
  		gBms_add=0xff;
 	}
   gCBUP.chg_value.temperature_inner=(uint16_t)gAdcValue[4];
   gCBUP.chg_value.temperature_outer=(uint16_t)gAdcValue[5];
   
   gCBUP.chg_value.voltage_inner=(  20 * ADC_FULL_SCALE * (uint16_t)gAdcValue[0] )/4095;
   gCBUP.chg_value.voltage_outer=( 20 * ADC_FULL_SCALE * (uint16_t)gAdcValue[1] )/4095;
   gCBUP.chg_value.current_monitor=( 10 * ADC_FULL_SCALE * (uint16_t)gAdcValue[2] )/4095; 
   gCBUP.chg_value.b.batt_exist=((uint16_t)gAdcValue[3]>0x20) ? 1:0;
  	
	ui.voltage=gCBUP.chg_value.voltage_inner;
	ui.current=gCBUP.chg_value.current_monitor;
	Drv_sps194_set();//relay,shutDown,fan
	Drv_solenoid_set_pos();
	Drv_as1107_StaticLED_loop();
	//printf("BAT_EXIST[%d]\r\n",gCBUP.chg_value.b.batt_exist);
}


int GetSerialBuf()
{
	int  i,d;
	int end=0;
	//for(i=0;i<10;i++)
	for(;;)
	{
		d =get();
		if(d==-1 )break;
			
		if(bms_frame==0){	
		//printf("GetSerialBuf  bms_data_cnt[%d][%02X]\r\n",bms_data_cnt, d);
				switch(d){
					case  0x75:
					case  0x02:
							bms_frame=1;
							bms_data_cnt=0;
							fsz=0xff;
							memset(bms_buf,0,sizeof(bms_buf));
						break;					
				}
		}
		if(bms_frame) {
				bms_buf[bms_data_cnt++]=d;
//				printf("GetSerialBuf  bms_data_cnt[%d][%02X]\r\n",bms_data_cnt, d);
			
				if(bms_data_cnt==3){
					 if( bms_buf[0]==0x02)	fsz=bms_buf[2]+5;
					 else	 if( bms_buf[0]==0x75)	fsz=23;
					// printf("bms_frame fsz[%d]\r\n",fsz);
				}
				if(bms_data_cnt>=fsz){
				//	printf("===>GetSerialBuf  bms_data_cnt[%d]\r\n",bms_data_cnt);
					Drv_bms_rcv();
	        		BMS_RCV_FLAG=0;
	        		bms_data_cnt=0;
					bms_frame=0;
				}
		}
		
		
		if(end) break;	
	}
}
*/