#include "stm32f1xx_hal.h"
#include "Drv_dcan.h"
#include "d_cecee.h"

extern CAN_HandleTypeDef hcan1;
extern uint8_t TransmitMailbox1;

DB_UPLOAD db_up;
CBD_DATA CbData[12];
uint8_t can_rcv_buf[12][16][8]={0,};

int Drv_dCan_init(void)
{
	Hal_CAN_Init();
  memset(&db_up,0,sizeof(db_up));
  memset(&CbData,0,sizeof(CbData));

  printf("DB_UPLOAD --size[%d] \r\n",sizeof(DB_UPLOAD));
  printf("D_STATUS --size[%d] \r\n",sizeof(D_STATUS));
  printf("CBD_DATA --size[%d] \r\n",sizeof(CBD_DATA));
   printf("db_up --[%d][%d][%d][%d] \r\n",db_up.dStatus.bFanAlarm.fan_f1,db_up.dStatus.bFanAlarm.fan_f2,db_up.dStatus.bFanAlarm.fan_f3,db_up.dStatus.bFanAlarm.fan_f4);
	return 1;
}

void Drv_CanSeqLoop()
{
	
      if(CAN_TX_FLAG<0x80)
      {
      // DBG(WARN,"CAN_TX_FLAG[%d]\n",CAN_TX_FLAG);
      switch(CAN_TX_FLAG)
      {
          case 12:
				//DBoard_GetCB_Exist(cb_exist);
				//memset(cb_exist,0,16);
				//DBoard_SetACMonitor(4);//For test
            break;
          case 13:
				//DBoard_SetACMonitor(0);//For test
				//DBoard_SendMessageDbdInfo();
            break;
          case 14:
				//DBoard_SetACMonitor(1);//For test
				//DBoard_SendMessageCbdAllInfo();
            break;				
          case 15:
				//DBoard_SetACMonitor(2);//For test
            break;	
          default:
          	//update to Mboard
            Drv_dbd_TXprocess(CAN_TX_FLAG);
          	Drv_CanRequest(CAN_TX_FLAG, 0x08);
            break;
	      }
        CAN_TX_FLAG=0x81;
     }   	
}

void Drv_SendmsgToCbd(uint32_t can_id, uint8_t *data)
{

 	//DBG(WARN,"Drv_SendmsgToCbd [%08X] [%x][%x]\n",can_id, data[0], data[1]);
 	__HAL_UNLOCK(&hcan1);	
	hcan1.pTxMsg->ExtId=can_id;
	memcpy(hcan1.pTxMsg->Data, data ,8);
	do
	{
		TransmitMailbox1 = HAL_CAN_Transmit(&hcan1, 10);
	}
	while( TransmitMailbox1 == CAN_TXSTATUS_NOMAILBOX );

}

void Drv_CanRequest(uint8_t id, uint8_t read_type)
{
	uint32_t cid;
	//CBD_CTRL_CMD *cccmd;
	uint8_t data[8]={0,};
	cid=0x0cecee00| ((id<<4)&0xf0)|(read_type&0x0f);//
//	memcpy(&data[0],&CCtrlCmd[id],8);
	Drv_SendmsgToCbd(cid, data);

}

void Drv_CanEventToC(uint8_t cid, uint8_t what, uint16_t value)
{
	uint32_t exid;
	uint8_t data[8]={0,};
  data[0]=what;
  data[1]=HI_UINT16(value);
  data[2]=LO_UINT16(value);
//	memcpy(&data[0],&BdCtrl.Ctrl_C,sizeof(BdCtrl.Ctrl_C));
	exid=0x0cecee00| ((cid<<4)&0xf0)|(0&0x0f);//
	Drv_SendmsgToCbd(exid, data);
}

void Drv_CanRx()
{
//	static uint8_t cnt=0;
//	uint8_t data[8];
//	uint8_t c_data[15][8];
	uint8_t packid, cbd, seq;
	uint32_t extid;
//	uint32_t exid_x;
	uint32_t mask=0x1cecee00;
	extid=hcan1.pRxMsg->ExtId;	
	packid=extid & ~(mask);
	cbd=(packid>>4)&0x0f;
	seq=packid&0x0f;
#if 0	
	if(seq==0){
 		DBG(WARN,"Drv_CanRx cbd[%X] seq[%d] cnt[%d]\n",cbd, seq, cnt);
		cnt=1;
	}
	else cnt++;
#endif		

 	//DBG(WARN,"Drv_CanRx cbd[%X] seq[%d]\n",cbd, seq);
	memcpy(&can_rcv_buf[cbd][seq],&hcan1.pRxMsg->Data,8);
	if(seq==0x0e){
		memcpy(&CbData[cbd],&can_rcv_buf[cbd],sizeof(CBD_DATA));
	  DBG(WARN,"Drv_CanRx cbd[%X] szCBD_DATA[%d]\n",cbd, sizeof(CBD_DATA));
	}
	//if(exid_x & ~(mask)
	

	
	//if(extid&(~0x1ceceef)==
	//cid=
//	ExtId=hcan1.pRxMsg->ExtId;
//	printf("extid[%08x] packid[%02x] [%x][%x]--\r\n",extid, packid, cbd, seq);
	
	
  	//memcpy(can1_rxbuf, hcan1.pRxMsg->Data,8);
  	
//	uint8_t myid=(myboard_id>>4)&0x0f;
//	uint8_t index=( (ExtId & 0xf0)>>4)&0x0f;
//	if(index!=myid) return 0;
//	uint32_t xx=ExtId&0xffffff00;	
	
//	can_rcv_buf
//	if(xx!=0x0cecee00) return 0;	

}

