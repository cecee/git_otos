#ifndef DRV_GPIO_H_
#define DRV_GPIO_H_

void Drv_gpio_getport(void);
void Drv_gpio_rgb(uint8_t pos, uint8_t color);
void Drv_gpio_fan(void);
#endif