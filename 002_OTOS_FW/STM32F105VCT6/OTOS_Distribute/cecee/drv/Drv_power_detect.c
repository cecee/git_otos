#include "stm32f1xx_hal.h"
#include "Drv_power_detect.h"
#include "d_cecee.h"

extern UART_HandleTypeDef huart3;

 uint8_t pmonitor_buf[8];
 
 uint8_t pmonitor_sbuf[8];
 uint8_t pmonitor_packet=0;
 uint8_t pmonitor_cnt=0;
 uint16_t pmonitor_packetSz=7;
 
 void GetACMonitor(uint8_t *data);
 
int GetSerialBuf_power_detect(void)
{
	int  i,d;
	int end=0;
//	for(i=0;i<10;i++)
	for(;;)
	{
		d =get_qUart3();
		if(d==-1 )break;
			
		if(d==0xa0 || d==0xa1 || d==0xa2 || d==0xa3){		
	  		if(pmonitor_packet==0){
	  			pmonitor_packet=1;
	 			pmonitor_cnt=0;
				pmonitor_sbuf[pmonitor_cnt++]=d;
	  		}
	  		else{
	  		 pmonitor_sbuf[pmonitor_cnt++]=d;
	  		}		
		}	
  		if(pmonitor_packet && (pmonitor_cnt>=pmonitor_packetSz)){
			pmonitor_packet=0;
			memcpy(pmonitor_buf, pmonitor_sbuf, 8);
			//for(int i=0;i<8;i++) printf("pmonitor_buf[%d][%02X]\r\n",i, pmonitor_buf[i]);
			memset(pmonitor_sbuf, 0, 8);
			GetACMonitor(pmonitor_buf);
		}			
		if(end) break;	
	}
  return 1;
}

void GetACMonitor(uint8_t *data)
{
    int i;
    uint16_t sum=0;
    uint16_t ivalue;
    
	for(i=0;i<6;i++) sum=sum+data[i];
	sum=sum&0xff;
	if(sum !=data[6]) return;
	switch(data[0])
	{
		case 0xA0://Get voltage
			ivalue=BUILD_UINT16(data[2], data[1]);
			db_up.dStatus.main_V=ivalue*10+data[3];
			break;	
		case 0xA1://Get current
			db_up.dStatus.main_A=(data[2]*10)+ data[3];
			break;			
		case 0xA2://Get Power
			db_up.dStatus.main_W=(data[1]*10)+ data[2];
			break;					
	}
	

}

void Drv_SetACMonitor(uint8_t kind )
{
	uint8_t buf[7]={0xb0, 0xc0, 0xa8,0x01, 0x01, 0x00, 0x1a};
	switch(kind)
	{
		case 0x00://Get voltage
			buf[0]=0xB0; buf[6]=0x1A; 
			break;			
		case 0x01://Get Current
			buf[0]=0xB1; buf[6]=0x1B; 
			break;	
		case 0x02://Get Average Power
			buf[0]=0xB2; buf[6]=0x1C; 
			break;				
		case 0x03://Get Read Energe
			buf[0]=0xB3; buf[6]=0x1D; 
			break;				
		case 0x04://Set module address
			buf[0]=0xB4; buf[6]=0x1E; 
			break;
		case 0x05://Set Power Alarm Threshold
			buf[0]=0xB5; buf[5]=0x14;buf[6]=0x33; 
			break;			
	
	}
	uint8_t size=7;
	uint16_t   time_out=(87*size*16)/1000; //ms 1byte를 16비트로 계산 (시간여유를 생각해서)
  	HAL_UART_Transmit(&huart3, buf, size, time_out);// 
}	
