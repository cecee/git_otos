#include "stm32f1xx_hal.h"
#include "Drv_gpio.h"
#include "d_cecee.h"

void Drv_gpio_getport(void)
{
	db_up.dStatus.bPsuAlarm.ac_fail=HAL_GPIO_ReadPin(GPIOC, AC_FAIL_Pin);
  db_up.dStatus.bPsuAlarm.dc_fail=HAL_GPIO_ReadPin(GPIOC, DC_FAIL_Pin);
  db_up.dStatus.bPsuAlarm.buckup_bat_fail=HAL_GPIO_ReadPin(GPIOC, BAT_FAIL_Pin);
  db_up.dStatus.bPsuAlarm.ovc=HAL_GPIO_ReadPin(GPIOC, OVC_Pin);
  
  db_up.dStatus.bFanAlarm.fan_f1=HAL_GPIO_ReadPin(GPIOC, FAN_F1_AL_Pin);
  db_up.dStatus.bFanAlarm.fan_f2=HAL_GPIO_ReadPin(GPIOC, FAN_F2_AL_Pin);
  db_up.dStatus.bFanAlarm.fan_f3=HAL_GPIO_ReadPin(GPIOB, FAN_F3_AL_Pin);
  db_up.dStatus.bFanAlarm.fan_f4=HAL_GPIO_ReadPin(GPIOB, FAN_F4_AL_Pin);
  db_up.dStatus.bFanAlarm.fan_r1=HAL_GPIO_ReadPin(GPIOC, FAN_R1_AL_Pin);
  db_up.dStatus.bFanAlarm.fan_r2=HAL_GPIO_ReadPin(GPIOC, FAN_R2_AL_Pin);
  db_up.dStatus.bFanAlarm.fan_r3=HAL_GPIO_ReadPin(GPIOC, FAN_R3_AL_Pin);
  db_up.dStatus.bFanAlarm.fan_r4=HAL_GPIO_ReadPin(GPIOC, FAN_R4_AL_Pin);
  
  db_up.dStatus.bSensor.door=HAL_GPIO_ReadPin(GPIOA, pDOOR_OC_Pin);
  db_up.dStatus.bSensor.shock1=HAL_GPIO_ReadPin(GPIOD, pSHOCK1_Pin);
  db_up.dStatus.bSensor.shock2=HAL_GPIO_ReadPin(GPIOD, pSHOCK2_Pin);
  db_up.dStatus.bSensor.slope1=HAL_GPIO_ReadPin(GPIOB, pSLOPE1_Pin);
  db_up.dStatus.bSensor.slope2=HAL_GPIO_ReadPin(GPIOB, pSLOPE2_Pin);
}

void Drv_gpio_rgb(uint8_t pos, uint8_t color)
{
	if(pos>5)return;
	switch(pos){
		case 0:
			switch(color){
				case 0:
					HAL_GPIO_WritePin(GPIOE, pR1_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG1_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB1_Pin,GPIO_PIN_RESET); 
					break;
				case 1://red
					HAL_GPIO_WritePin(GPIOE, pR1_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pG1_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB1_Pin,GPIO_PIN_RESET);					
					break;		
				case 2://green
					HAL_GPIO_WritePin(GPIOE, pR1_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG1_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pB1_Pin,GPIO_PIN_RESET);					
					break;
				case 3://blue
					HAL_GPIO_WritePin(GPIOE, pR1_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG1_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB1_Pin,GPIO_PIN_SET);					
					break;							
			}
			break;
		case 1:
			switch(color){
				case 0:
					HAL_GPIO_WritePin(GPIOE, pR2_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG2_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB2_Pin,GPIO_PIN_RESET); 
					break;
				case 1://red
					HAL_GPIO_WritePin(GPIOE, pR2_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pG2_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB2_Pin,GPIO_PIN_RESET);					
					break;		
				case 2://green
					HAL_GPIO_WritePin(GPIOE, pR2_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG2_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pB2_Pin,GPIO_PIN_RESET);					
					break;
				case 3://blue
					HAL_GPIO_WritePin(GPIOE, pR2_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG2_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB2_Pin,GPIO_PIN_SET);					
					break;							
			}
			break;
		case 2:
			switch(color){
				case 0:
					HAL_GPIO_WritePin(GPIOE, pR3_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG3_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB3_Pin,GPIO_PIN_RESET); 
					break;
				case 1://red
					HAL_GPIO_WritePin(GPIOE, pR3_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pG3_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB3_Pin,GPIO_PIN_RESET);					
					break;		
				case 2://green
					HAL_GPIO_WritePin(GPIOE, pR3_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG3_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pB3_Pin,GPIO_PIN_RESET);					
					break;
				case 3://blue
					HAL_GPIO_WritePin(GPIOE, pR3_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG3_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB3_Pin,GPIO_PIN_SET);					
					break;							
			}
			break;
		case 3:
			switch(color){
				case 0:
					HAL_GPIO_WritePin(GPIOE, pR4_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG4_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB4_Pin,GPIO_PIN_RESET); 
					break;
				case 1://red
					HAL_GPIO_WritePin(GPIOE, pR4_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pG4_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB4_Pin,GPIO_PIN_RESET);					
					break;		
				case 2://green
					HAL_GPIO_WritePin(GPIOE, pR4_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG4_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pB4_Pin,GPIO_PIN_RESET);					
					break;
				case 3://blue
					HAL_GPIO_WritePin(GPIOE, pR4_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG4_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB4_Pin,GPIO_PIN_SET);					
					break;							
			}
			break;
		case 4:
			switch(color){
				case 0:
					HAL_GPIO_WritePin(GPIOE, pR5_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG5_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB5_Pin,GPIO_PIN_RESET); 
					break;
				case 1://red
					HAL_GPIO_WritePin(GPIOE, pR5_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pG5_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB5_Pin,GPIO_PIN_RESET);					
					break;		
				case 2://green
					HAL_GPIO_WritePin(GPIOE, pR5_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG5_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOE, pB5_Pin,GPIO_PIN_RESET);					
					break;
				case 3://blue
					HAL_GPIO_WritePin(GPIOE, pR5_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pG5_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOE, pB5_Pin,GPIO_PIN_SET);					
					break;							
			}
			break;
		case 5:
			switch(color){
				case 0:
					HAL_GPIO_WritePin(GPIOB, pR6_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOB, pG6_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOB, pB6_Pin,GPIO_PIN_RESET); 
					break;
				case 1://red
					HAL_GPIO_WritePin(GPIOB, pR6_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOB, pG6_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOB, pB6_Pin,GPIO_PIN_RESET);					
					break;		
				case 2://green
					HAL_GPIO_WritePin(GPIOB, pR6_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOB, pG6_Pin,GPIO_PIN_SET); 
					HAL_GPIO_WritePin(GPIOB, pB6_Pin,GPIO_PIN_RESET);					
					break;
				case 3://blue
					HAL_GPIO_WritePin(GPIOB, pR6_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOB, pG6_Pin,GPIO_PIN_RESET); 
					HAL_GPIO_WritePin(GPIOB, pB6_Pin,GPIO_PIN_SET);					
					break;							
			}
			break;									
	}

}

void Drv_gpio_fan()
{
	if(BdCtrl.Ctrl_D.bCtrl.fan_f1)	HAL_GPIO_WritePin(GPIOD, pFAN_F1__Pin,GPIO_PIN_SET);					
  else HAL_GPIO_WritePin(GPIOD, pFAN_F1__Pin,GPIO_PIN_RESET);					
	
  if(BdCtrl.Ctrl_D.bCtrl.fan_f2)	HAL_GPIO_WritePin(GPIOD, pFAN_F2__Pin,GPIO_PIN_SET);					
  else HAL_GPIO_WritePin(GPIOD, pFAN_F2__Pin,GPIO_PIN_RESET);					
	
  if(BdCtrl.Ctrl_D.bCtrl.fan_f3)	HAL_GPIO_WritePin(GPIOD, pFAN_F3__Pin,GPIO_PIN_SET);					
  else HAL_GPIO_WritePin(GPIOD, pFAN_F3__Pin,GPIO_PIN_RESET);					
	
  if(BdCtrl.Ctrl_D.bCtrl.fan_f4)	HAL_GPIO_WritePin(GPIOD, pFAN_F4__Pin,GPIO_PIN_SET);					
  else HAL_GPIO_WritePin(GPIOD, pFAN_F4__Pin,GPIO_PIN_RESET);					
	
  
  if(BdCtrl.Ctrl_D.bCtrl.fan_r1)	HAL_GPIO_WritePin(GPIOD, pFAN_R1__Pin,GPIO_PIN_SET);					
  else HAL_GPIO_WritePin(GPIOD, pFAN_R1__Pin,GPIO_PIN_RESET);					
	if(BdCtrl.Ctrl_D.bCtrl.fan_r2)	HAL_GPIO_WritePin(GPIOD, pFAN_R2__Pin,GPIO_PIN_SET);					
  else HAL_GPIO_WritePin(GPIOD, pFAN_R2__Pin,GPIO_PIN_RESET);					
	if(BdCtrl.Ctrl_D.bCtrl.fan_r3)	HAL_GPIO_WritePin(GPIOD, pFAN_R3__Pin,GPIO_PIN_SET);					
  else HAL_GPIO_WritePin(GPIOD, pFAN_R3__Pin,GPIO_PIN_RESET);					
	if(BdCtrl.Ctrl_D.bCtrl.fan_r4)	HAL_GPIO_WritePin(GPIOD, pFAN_R4__Pin,GPIO_PIN_SET);					
  else HAL_GPIO_WritePin(GPIOD, pFAN_R4__Pin,GPIO_PIN_RESET);					
  
}