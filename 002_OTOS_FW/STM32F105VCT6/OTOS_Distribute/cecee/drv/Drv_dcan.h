/*
Drv_dcan.h
*/
#ifndef DRV_DCAN_H_
#define DRV_DCAN_H_
#include <stdint.h>

typedef struct dbd_th11_tag_bit
{
	//signed integer
	int16_t temper;
	int16_t humidi;
}DHT11;

typedef struct dbd_relay_tag_bit
{
	uint8_t ac_light: 1;
	uint8_t ac_monitor: 1;	
	uint8_t ac_heaterA: 1;	
	uint8_t ac_heaterB: 1;	
}RELAY_BIT;

typedef struct dbd_fan_tag_bit
{
	uint8_t fan_f1: 1;
	uint8_t fan_f2: 1;	
	uint8_t fan_f3: 1;	
	uint8_t fan_f4: 1;	
  uint8_t fan_r1: 1;
	uint8_t fan_r2: 1;	
	uint8_t fan_r3: 1;	
	uint8_t fan_r4: 1;	
}FAN_BIT;


typedef struct dbd_ssensor_tag_bit
{
	uint16_t door: 1;	
	uint16_t slope1: 1;
	uint16_t slope2: 1;	
	uint16_t shock1: 1;	
	uint16_t shock2: 1;		
	uint16_t nfc_led: 1;	
	uint16_t mood: 1;
 	uint16_t dht1: 1;
	uint16_t dht2: 1;
	uint16_t dht3: 1; 
}SENSOR_BIT;

typedef struct dbd_pahalarm_tag_bit
{
	uint8_t ac_fail: 1;	
	uint8_t dc_fail: 1;
	uint8_t buckup_bat_fail: 1;	
	uint8_t ovc: 1;	
}PSU_ALARM_BIT;

typedef struct tagbdStatusport
{
	RELAY_BIT bRelay;
	FAN_BIT bFanCtrl;
  FAN_BIT bFanAlarm;
	SENSOR_BIT bSensor;
	PSU_ALARM_BIT bPsuAlarm;
	DHT11 dht[3];
	uint8_t buckRGB[6];
	int16_t tmp36_0;
	int16_t tmp36_1;
	uint16_t cb_exist;
	uint16_t cds;
	uint16_t dimmer;
	uint16_t main_V;
	uint16_t main_A;
	uint16_t main_W;
}D_STATUS;

//------------------------------
typedef struct tag_solBit{
	uint16_t typel_sol: 1;
	uint16_t ctrl_d1: 1;
	uint16_t ctrl_d2: 1;
	uint16_t ctrl_b1: 1;
	uint16_t ctrl_b2: 1;
	uint16_t ctrl_htr: 1;
	uint16_t ctrl_relay: 1;
	uint16_t ctrl_sd: 1;
	uint16_t ctrl_fan: 1;	
	uint16_t x9: 1;	
	uint16_t x10: 1;	
	uint16_t x11: 1;	
	uint16_t x12: 1;	
	uint16_t x13: 1;	
	uint16_t x14: 1;			
	uint16_t x15: 1;						
}BIT_CB_CTRL;

typedef union tag_solBit_union
{
	uint16_t value;
	BIT_CB_CTRL b;
}UNION_CB_CTRL;

typedef struct _CONTROL_CBD
{
	uint8_t cid;//1
	UNION_CB_CTRL CbCtrl;//2
	uint16_t MaxV;//2
	uint16_t MaxA;//2
	uint8_t x7;
}CONTROL_CBD;


//-------------------------------------
typedef struct tag_solCtrlBit{
  uint8_t sol_type: 1;
	uint8_t bHTR: 1;  
	uint8_t D1: 1;
	uint8_t D2: 1;
	uint8_t B1: 1;
	uint8_t B2: 1;
}BIT_SOLENOID_CTRL;

typedef struct tag_solPosBit{
	uint8_t D1: 2;
	uint8_t D2: 2;
	uint8_t B1: 2;
	uint8_t B2: 2;
}BIT_SOLENOID_POS;

typedef struct tag_sol_status{
	BIT_SOLENOID_CTRL bSolCtrl;
	BIT_SOLENOID_POS bSolPos;
}SOLENOID_VALUE;

//BMS------------------------------------

typedef struct tag_bms_err_bit
{
	uint32_t uv_warning: 1;
	uint32_t uv_error: 1;
	uint32_t ov_warning: 1;		
	uint32_t ov_error: 1;
	uint32_t ot_warning: 1;
	uint32_t ot_error: 1;
	uint32_t ut_warning: 1;		
	uint32_t ut_error: 1;	
			
	uint32_t oc_warning_chg: 1;		
	uint32_t oc_error_chg: 1;     
	uint32_t oc_warning_dis: 1;
	uint32_t oc_error_dis: 1;	
	uint32_t comm_error: 1;	
	uint32_t by1bit5: 1;		
	uint32_t by1bit6: 1;		
	uint32_t by1bit7: 1;
		
	uint32_t pack_uv_warning: 1;		
	uint32_t pack_uv_protect: 1;     
	uint32_t pack_ov_warning: 1;
	uint32_t pack_ov_protect: 1;	
	uint32_t by2bit4: 1;	
	uint32_t by2bit5: 1;		
	uint32_t by2bit6: 1;		
	uint32_t by2bit7: 1;	
				
	uint32_t by3bit0: 1;		
	uint32_t by3bit1: 1;     
	uint32_t by3bit2: 1;
	uint32_t by3bit3: 1;	
	uint32_t by3bit4: 1;	
	uint32_t by3bit5: 1;		
	uint32_t warning: 1;		
	uint32_t error: 1;	
}BMS_ERR_BIT;


typedef struct tag_regacy_bms_err_bit
{
	uint16_t fault_dis_htemp: 1;
	uint16_t fault_dis_ltemp: 1;
	uint16_t fault_cha_htemp: 1;
	uint16_t fault_cha_ltemp: 1;
	uint16_t fault_cha_ocurr: 1;
	uint16_t fault_dis_ocurr: 1;
	uint16_t fault_singlecell_lvolt: 1;
	uint16_t fault_singlecell_hvolt: 1;
//--------------------------------------------		
	uint16_t fault_singlecell_dvolt: 1;
	uint16_t x9: 1;
	uint16_t x10: 1;
	uint16_t x11: 1;
	uint16_t x12: 1;
	uint16_t x13: 1;
	uint16_t x14: 1;
	uint16_t x15: 1;		
}REGACY_BMS_ERR_BIT;

typedef union tag_uni_regacy_err
{
	uint16_t value;
	REGACY_BMS_ERR_BIT b;
}BMS_REGACY_ERR;

typedef union tag_bms_error_union
{
	uint8_t value[4];
	BMS_ERR_BIT b;
}BMS_ERR;

typedef union _CELL_BAL
{
	uint8_t value[4];
}CELL_BAL;

typedef union _USER_DATA
{
	uint8_t value[3];
}USER_DATA;

typedef union _PACK_COUNTER
{
	uint8_t value[6];
}PACK_COUNTER;

typedef struct tag_bms_value
{
  uint16_t cell_votage[20];//cell_votage[16];
	int16_t cell_temper[6];//"-"온도 있을수 있음
	uint16_t pack_voltage;
	int16_t pack_current; //"-"값 있음 전류방향에 따라서
	uint16_t pack_soc;//new: 2byte  old:1byte
	int16_t pack_temper;//(58)통신[60]MSB
	
	uint8_t pack_serial;//추가[62]
	uint8_t pack_parallel;//추가
	uint8_t pack_capacity;
	uint8_t pack_year;
	uint8_t pack_month;
	uint8_t pack_day;
	uint16_t pack_nation;
	uint16_t pack_serialNumber;//(68)통신[70-lsb][71-msb]
	uint16_t pack_cycle;//[72-lsb][73-msb]
	USER_DATA pack_userData;//[74-lsb][76-msb]	
	PACK_COUNTER pack_Counter;//[77]-[82]
  CELL_BAL pack_cellBancing;//83(MSB) .. 86(LSB)//추가	
	BMS_ERR pack_err; //[90-[93]
	BMS_REGACY_ERR regacy_pack_err;
}BMS_VALUE;
//----------------------------------

typedef struct tag_status_bit
{
	uint16_t pre_charging: 1;
	uint16_t cc_charging: 1;
	uint16_t cv_charging: 1;
	uint16_t batt_exist: 1;
	uint16_t x4: 1;
	uint16_t ac_fail: 1;
	uint16_t tmp_fail: 1;
	uint16_t fan_fail: 1;
	uint16_t dc_fail: 1;
	uint16_t load_balance: 1;	
	uint16_t relay: 1;	
	uint16_t dc_sd: 1;		
	uint16_t ctrl_fan: 1;		
	uint16_t fail_all: 1;		
	uint16_t x14: 1;		
	uint16_t x15: 1;			
}SPS_STATUS_BIT;

typedef struct tag_sps_value
{
	uint16_t voltage_setting;
	uint16_t voltage_outer;
	uint16_t voltage_inner;	
	uint16_t current_setting;
	uint16_t current_monitor;
	uint16_t temperature_inner;
	uint16_t temperature_outer;
	uint16_t cid;
	uint16_t cversion;
	SPS_STATUS_BIT b;
}SPS_VALUE;


typedef struct tag_cbupload
{
	BMS_VALUE bms_value;
	SPS_VALUE chg_value;
	SOLENOID_VALUE sol_value;
}CBD_DATA;


typedef struct tag_db_upload
{
	uint8_t sof;
	uint8_t cid;
  uint16_t size;
	D_STATUS dStatus;
	CBD_DATA CbData;
	uint16_t crc;
	uint16_t eof;
}DB_UPLOAD;


void Drv_CanSeqLoop(void);
void Drv_CanRequest(uint8_t id, uint8_t read_type);
void Drv_SendmsgToCbd(uint32_t can_id, uint8_t *data);  
void Drv_CanRx(void);
void Drv_CanEventToC(uint8_t cid, uint8_t what, uint16_t value);


#endif