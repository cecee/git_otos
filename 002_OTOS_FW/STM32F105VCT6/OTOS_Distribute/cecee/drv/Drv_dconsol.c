#include "stm32f1xx_hal.h"
#include "Drv_dconsol.h"
#include "d_cecee.h"
//uint8_t update_ready;

BD_CONTROL BdCtrl;

void Drv_dbd_view(void){
	 DBG(WARN,"==========================================================\n");
   DBG(MSG,"ac_heaterA[%d] ac_heaterA[%d]   ac_light[%d]   ac_monitor[%d]\n",db_up.dStatus.bRelay.ac_heaterA,db_up.dStatus.bRelay.ac_heaterB,db_up.dStatus.bRelay.ac_light,db_up.dStatus.bRelay.ac_monitor);
   DBG(MSG,"ctrlfan_f1[%d] ctrlfan_f2[%d] ctrlfan_f3[%d] ctrlfan_f4[%d]\n",db_up.dStatus.bFanCtrl.fan_f1,db_up.dStatus.bFanCtrl.fan_f2,db_up.dStatus.bFanCtrl.fan_f3,db_up.dStatus.bFanCtrl.fan_f4);
   DBG(MSG,"ctrlfan_r1[%d] ctrlfan_r2[%d] ctrlfan_r3[%d] ctrlfan_r4[%d]\n",db_up.dStatus.bFanCtrl.fan_r1,db_up.dStatus.bFanCtrl.fan_r2,db_up.dStatus.bFanCtrl.fan_r3,db_up.dStatus.bFanCtrl.fan_r4);
   DBG(MSG,"alarfan_f1[%d] alarfan_f2[%d] alarfan_f3[%d] alarfan_f4[%d]\n",db_up.dStatus.bFanAlarm.fan_f1,db_up.dStatus.bFanAlarm.fan_f2,db_up.dStatus.bFanAlarm.fan_f3,db_up.dStatus.bFanAlarm.fan_f4);
   DBG(MSG,"alarfan_r1[%d] alarfan_r2[%d] alarfan_r3[%d] alarfan_r4[%d]\n",db_up.dStatus.bFanAlarm.fan_r1,db_up.dStatus.bFanAlarm.fan_r2,db_up.dStatus.bFanAlarm.fan_r3,db_up.dStatus.bFanAlarm.fan_r4);
   DBG(MSG,"      dht1[%d]       dht2[%d]       dht3[%d]\n",db_up.dStatus.bSensor.dht1,db_up.dStatus.bSensor.dht2,db_up.dStatus.bSensor.dht3);
   DBG(MSG,"    shock1[%d]     shock2[%d]     slope1[%d]     slope2[%d]\n",db_up.dStatus.bSensor.shock1,db_up.dStatus.bSensor.shock2,db_up.dStatus.bSensor.slope1,db_up.dStatus.bSensor.slope2);
   DBG(MSG,"   ac_fail[%d]    dc_fail[%d]        ovc[%d]   bat_fail[%d]\n",db_up.dStatus.bPsuAlarm.ac_fail,db_up.dStatus.bPsuAlarm.dc_fail,db_up.dStatus.bPsuAlarm.ovc,db_up.dStatus.bPsuAlarm.buckup_bat_fail);
   DBG(MSG,"   Temper1[%d]    Temper2[%d]    Temper3[%d]  \n",db_up.dStatus.dht[0].temper,db_up.dStatus.dht[1].temper,db_up.dStatus.dht[2].temper);
   DBG(MSG,"   Humidi1[%d]    Humidi2[%d]    Humidi3[%d]  \n",db_up.dStatus.dht[0].humidi,db_up.dStatus.dht[1].humidi,db_up.dStatus.dht[2].humidi);
   DBG(MSG,"   RGB[%02X] [%02X] [%02X] [%02X] [%02X] [%02X]  \n",db_up.dStatus.buckRGB[0],db_up.dStatus.buckRGB[1],db_up.dStatus.buckRGB[2],db_up.dStatus.buckRGB[3],db_up.dStatus.buckRGB[4],db_up.dStatus.buckRGB[5]);
   DBG(MSG,"    Tmp36A[%d]    Tmp36B[%d]\n",db_up.dStatus.tmp36_0,db_up.dStatus.tmp36_1);
   DBG(MSG,"Cboard_Exist[%04X]\n",db_up.dStatus.cb_exist);
   DBG(MSG,"     CDS[%04X]    DIMMER[%04X]\n",db_up.dStatus.cds,db_up.dStatus.dimmer);
   DBG(MSG,"  main_V[%04d]    main_V[%04d] main_W[%04d]\n",db_up.dStatus.main_V,db_up.dStatus.main_A, db_up.dStatus.main_W);
   DBG(WARN,"==========================================================\n");
//   DBG(MSG,"  0-voltage[%04d] 1-voltage[%04d] mood[%d]\n",BdCtrl.Ctrl_C.voltage_setting, BdCtrl.Ctrl_C.voltage_setting, BdCtrl.Ctrl_D.bCtrl.mood);
}

void Drv_dbd_cinfo(uint8_t cid)
{
	DBG(WARN,"==========================================================\n");
//	DBG(MSG,"pack_voltage[%d] pack_current[%d]  pack_soc[%d]\n",CbData[cid].bms_value.pack_voltage,CbData[cid].bms_value.pack_current, CbData[cid].bms_value.pack_soc);
	DBG(WARN,"==========================================================\n");
  if(cid==0)Drv_dbd_controll();
  else 	Drv_dbd_controll_off();
}

void Drv_dbd_TXprocess(uint8_t cid)
{
  int i,offset;
  //uint8_t *data;
  uint8_t sum=0;
  uint16_t sz=0;
//uint8_t test[10];
  uint8_t data[200];
  
 	db_up.sof='!';
  db_up.eof='@';
  db_up.cid=cid;
  db_up.size=sizeof(DB_UPLOAD);
  sz= db_up.size;
  
  //data=(uint8_t *)malloc(sz+10);
 // if(data==0){  printf("[%d]malloc error!! \r\n",sz);   return;}

		// printf("BdCtrl --size[%d] \r\n",sizeof(BdCtrl));
		// printf("CB_UPLOAD --size[%d] \r\n",sizeof(CB_UPLOAD));
		//D_STATUS dStatus;
		//CB_UPLOAD CbData[1];
		// db_up.dStatus.bRelay.ac_heaterA=1;
		// db_up.dStatus.bRelay.ac_heaterB=1;
		// db_up.dStatus.bFanCtrl.fan_f1=1;
		// db_up.dStatus.bFanCtrl.fan_f4=1;
		// db_up.dStatus.bFanAlarm.fan_f2=0;
		// db_up.dStatus.bFanAlarm.fan_f3=0;
		// db_up.dStatus.bSensor.door=1;
		// db_up.dStatus.bSensor.dht3=1;
		//   DBG(WARN,"dht1[%d] dht2[%d] dht3[%d]\n",db_up.dStatus.bSensor.dht1,db_up.dStatus.bSensor.dht2,db_up.dStatus.bSensor.dht3);

		db_up.dStatus.buckRGB[0]=0xa0;
		db_up.dStatus.buckRGB[1]=0xa1;
		db_up.dStatus.buckRGB[2]=0xa2;
		db_up.dStatus.buckRGB[3]=0xa3;
		db_up.dStatus.buckRGB[4]=0xa4;
		db_up.dStatus.buckRGB[5]=0xa5;
		//    db_up.dStatus.bSensor.dht2=1;
		//   db_up.dStatus.bSensor.dht3=1;
		db_up.dStatus.bPsuAlarm.buckup_bat_fail=0;
		db_up.dStatus.dimmer=4000;
		db_up.dStatus.main_W=3300;
		//CbData[0].bms_value.cell_votage[0]=1234;
		db_up.CbData.bms_value.pack_serial=0xAA;

		db_up.CbData.bms_value.pack_err.value[0]=0xe0;
		db_up.CbData.bms_value.pack_err.value[1]=0xe1;
		db_up.CbData.bms_value.pack_err.value[2]=0xe2;
		db_up.CbData.bms_value.pack_err.value[3]=0xe3;
		db_up.CbData.bms_value.regacy_pack_err.value=0xefee;
		db_up.CbData.chg_value.voltage_setting=0xc001;
		db_up.CbData.chg_value.b.fail_all=1;
		db_up.CbData.sol_value.bSolCtrl.sol_type=1;
		db_up.CbData.sol_value.bSolCtrl.bHTR=1;
		db_up.CbData.sol_value.bSolPos.D1=0;
		db_up.CbData.sol_value.bSolPos.D2=2;
		db_up.CbData.sol_value.bSolPos.B1=3;
		db_up.CbData.sol_value.bSolPos.B2=1;    
    
  memcpy(&data,&db_up,sz);
  for(i=0;i<sz;i++){
  	sum=(sum+data[i])&0xff;
  } 
	db_up.crc=sum;
	memset(&data[0],0,sz);
  
	memcpy(&data[0],&db_up,sz);
//	printf("malloc --db_up.eof[%x] sz[%d] cid[%d] [%x][%x][%x]\r\n",db_up.eof, sz, db_up.cid, data[0],data[1],data[2]);
//	offset=160;
//  for(i=0;i<30;i++) printf("[%d] [%x]\r\n",i+offset, data[offset+i+0]);
	
  Hal_Uart_TxtoM(&data, sz+1);
#if 0
	//	Hal_Uart_TxtoM(data, sz+1);
	
		test[0]='!';
		test[1]=0x31;
		test[2]=0x32;
		test[3]=0x33;
		test[4]=0x0a;
		test[5]='@';
		Hal_Uart_TxtoM(test, 8);
 
#endif
 //	free(data);
//  update_ready=1;
}

void Drv_dbd_controll()
{
	uint8_t i;
  
  BdCtrl.Ctrl_C.cid=8;
	BdCtrl.Ctrl_D.buckRGB[0]=3;
	BdCtrl.Ctrl_D.buckRGB[5]=1;
  
  BdCtrl.Ctrl_D.bCtrl.fan_f1=1;
  BdCtrl.Ctrl_D.bCtrl.fan_f2=0;
  BdCtrl.Ctrl_D.bCtrl.fan_f3=1;
  BdCtrl.Ctrl_D.bCtrl.fan_f4=0;
  BdCtrl.Ctrl_D.bCtrl.fan_r1=1;
  BdCtrl.Ctrl_D.bCtrl.fan_r2=1;
  BdCtrl.Ctrl_D.bCtrl.fan_r3=1;
  BdCtrl.Ctrl_D.bCtrl.fan_r4=1;
  
  
  BdCtrl.Ctrl_C.CbCtrl.b.ctrl_d1=1;
  BdCtrl.Ctrl_C.CbCtrl.b.ctrl_d2=1;
  BdCtrl.Ctrl_C.CbCtrl.b.ctrl_relay=1;
	///RGB Control on DBD
	for(i=0;i<6;i++) Drv_gpio_rgb(i, BdCtrl.Ctrl_D.buckRGB[i] );
	///Cbd Control
  Drv_gpio_fan();
	//Drv_CanEventToC();
	
}

void Drv_dbd_controll_off()
{
	uint8_t i;
  
  BdCtrl.Ctrl_C.cid=8;
	BdCtrl.Ctrl_D.buckRGB[0]=3;
	BdCtrl.Ctrl_D.buckRGB[5]=1;
  
  BdCtrl.Ctrl_D.bCtrl.fan_f1=0;
  BdCtrl.Ctrl_D.bCtrl.fan_f2=0;
  BdCtrl.Ctrl_D.bCtrl.fan_f3=0;
  BdCtrl.Ctrl_D.bCtrl.fan_f4=0;
  BdCtrl.Ctrl_D.bCtrl.fan_r1=0;
  BdCtrl.Ctrl_D.bCtrl.fan_r2=0;
  BdCtrl.Ctrl_D.bCtrl.fan_r3=0;
  BdCtrl.Ctrl_D.bCtrl.fan_r4=0;
  
  
  BdCtrl.Ctrl_C.CbCtrl.b.ctrl_d1=0;
  BdCtrl.Ctrl_C.CbCtrl.b.ctrl_d2=0;
  BdCtrl.Ctrl_C.CbCtrl.b.ctrl_relay=0;
	///RGB Control on DBD
	for(i=0;i<6;i++) Drv_gpio_rgb(i, BdCtrl.Ctrl_D.buckRGB[i] );
	///Cbd Control
  Drv_gpio_fan();
	//Drv_CanEventToC();
	
}

void Drv_Control_IO(uint8_t *data)
{
	uint8_t i;
  uint8_t bd,cid,what;
  uint16_t value;
  
  uint8_t buf[3];
  memset(buf,0,3);
  buf[0]='0';  buf[1]=data[1];  buf[2]=data[2];
  bd=atoi(buf);
  
  memset(buf,0,3);
  buf[0]='0';  buf[1]=data[3];  buf[2]=data[4];
  cid=atoi(buf);
 
  memset(buf,0,3);
  buf[0]='0';  buf[1]=data[5];  buf[2]=data[6];
  what=atoi(buf);  
 
  memset(buf,0,3);
  buf[0]=data[7];  buf[1]=data[8];  buf[2]=data[9];
  value=atoi(buf);  
  
  if(bd==0)//d-board
  {
    
    if(what<6){//LED
      BdCtrl.Ctrl_D.buckRGB[what]=value;
      Drv_gpio_rgb(what, value );
    }
    
    switch(what)
    {
      case 6: //Front Fan
        BdCtrl.Ctrl_D.bCtrl.fan_f1=value;
        BdCtrl.Ctrl_D.bCtrl.fan_f2=value;
        BdCtrl.Ctrl_D.bCtrl.fan_f3=value;
        BdCtrl.Ctrl_D.bCtrl.fan_f4=value;
        Drv_gpio_fan();
        break;
      case 7: //Rear Fan
        BdCtrl.Ctrl_D.bCtrl.fan_r1=value;
        BdCtrl.Ctrl_D.bCtrl.fan_r2=value;
        BdCtrl.Ctrl_D.bCtrl.fan_r3=value;
        BdCtrl.Ctrl_D.bCtrl.fan_r4=value;  
        Drv_gpio_fan();
        break;        
    }
  
  }
  else
  {
     Drv_CanEventToC(cid, what, value);
  }
  
  
  printf("Drv_Control_IO bd[%d]\n",bd);
  
	//Drv_CanEventToC();
	
}