#include "stm32f1xx_hal.h"
#include "Drv_dht11.h"
#include "d_cecee.h"

uint8_t th_irq_cnt[3];
uint16_t th_irq_buf[3][60];

void init_dth11(void){
	memset(th_irq_cnt,0,sizeof(th_irq_cnt));
	memset(th_irq_buf,0,sizeof(th_irq_buf));
}


void Drv_DhtStartInterrup(void)
{
	GPIO_InitTypeDef GPIO_Struct;
	GPIO_Struct.Pin = DHT11_S1_Pin|DHT11_S2_Pin|DHT11_S3_Pin;
 	GPIO_Struct.Mode = GPIO_MODE_IT_FALLING;
  	GPIO_Struct.Pull = GPIO_PULLUP;
  	HAL_GPIO_Init(GPIOC, &GPIO_Struct);
}

void Drv_DhtReadValue(void)
{
	//printf("#######DHT_Read-----th_irq_cnt[%d][%d][%d]\r\n", th_irq_cnt[0], th_irq_cnt[1], th_irq_cnt[2]);
	uint8_t i,t;
	uint8_t offset=3;
	uint32_t th_data=0;
	uint8_t data[4];
	uint8_t th_sum=0;
	uint8_t data_sum=0;
	uint8_t th_bit;
	int16_t val16;
	int16_t val;
	
	for(t=0;t<3;t++)
	{
//------------------------------------------	
		th_sum=data_sum=0;
		th_data=0;
		//printf("th_irq_cnt[%d][%d]\r\n",  t, th_irq_cnt[t]); 
		
		//if(th_irq_cnt[t]>=43) for(i=0;i<43;i++) printf("[%d]th_irq_buf[%d]-th_irq_cnt[%d]\r\n", i, t, th_irq_buf[t][i]); 
		if(th_irq_cnt[t]>=43){
			
			for(i=0;i<40;i++){
				if(th_irq_buf[t][offset+i]>10 && th_irq_buf[t][offset+i]<20) th_bit=0;//6~10
				else if( th_irq_buf[t][offset+i] >= 21 && th_irq_buf[t][offset+i]<30) th_bit=1;//11~16
				else break;
				if(i<32){
					th_data=th_data<<1;
					th_data=	th_data+th_bit;
				}
				else {
					th_sum=th_sum<<1;
					th_sum=	th_sum+th_bit;
				}
					
			}
			data[0]=(th_data>>24)&0xff ;
			data[1]=(th_data>>16)&0xff ;
			data[2]=(th_data>>8)&0xff ;
			data[3]=th_data&0xff ;
			//printf("t[%d]-[%08x] [%x][%x][%x][%x]-th_sum[%x]\r\n",t, th_data, data[0],data[1],data[2],data[3], th_sum);
		
			data_sum=(data[0]+data[1]+data[2]+data[3])&0xff;
			if(data_sum==th_sum){
				val16=	BUILD_UINT16(data[3], data[2]);	
				if(val16&0x8000) val=(val16&0x7fff)*(-1); else val=val16;
				db_up.dStatus.dht[t].temper=val; 
				
				val16=	BUILD_UINT16(data[1], data[0]);	
				if(val16&0x8000) val=(val16&0x7fff)*(-1); else val=val16;
				db_up.dStatus.dht[t].humidi=val;
        if(t==0) db_up.dStatus.bSensor.dht1=1;
        else if(t==1)db_up.dStatus.bSensor.dht2=1;
        else if(t==2)db_up.dStatus.bSensor.dht3=1;
			}	
		}
		else 
		{
      if(t==0) db_up.dStatus.bSensor.dht1=0;
      else if(t==1)db_up.dStatus.bSensor.dht2=0;
      else if(t==2)db_up.dStatus.bSensor.dht3=0;	
			db_up.dStatus.dht[t].temper=-999; 
			db_up.dStatus.dht[t].humidi=-999;
		}
		th_irq_cnt[t]=0;
		memset(th_irq_buf[t],0, sizeof(th_irq_buf[t]));

//---------------------------------------------------	
	}
	//printf("THstatus[%d][%d][%d]\r\n",status_cur.status.b.th_status1 , status_cur.status.b.th_status2,status_cur.status.b.th_status3);


	GPIO_InitTypeDef GPIO_Struct;
 
  GPIO_Struct.Pin = DHT11_S1_Pin|DHT11_S2_Pin|DHT11_S3_Pin;
	GPIO_Struct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_Struct.Pull = GPIO_PULLUP;
	GPIO_Struct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOC, &GPIO_Struct);	
	
	HAL_GPIO_WritePin(GPIOC, DHT11_S1_Pin,GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOC, DHT11_S2_Pin,GPIO_PIN_RESET); 
	HAL_GPIO_WritePin(GPIOC, DHT11_S3_Pin,GPIO_PIN_RESET);  
	th_start=1;
  th_start_tick=0;
//	printf("#######DHT_Read-----temper[%d][%d][%d]\r\n", dStatus.dht[0].temper, dStatus.dht[1].temper,dStatus.dht[2].temper);
//	printf("#######DHT_Read-----humidi[%d][%d][%d]\r\n", dStatus.dht[0].humidi, dStatus.dht[1].humidi, dStatus.dht[2].humidi);

}