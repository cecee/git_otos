/*
Drv_dconsole.h
*/
#ifndef DRV_DCONSOLE_H_
#define DRV_DCONSOLE_H_

#include "drv_dcan.h"

typedef struct tag_dcontrol_bit
{
    uint16_t ac_light: 1;
    uint16_t ac_monitor: 1;
    uint16_t ac_heaterA: 1;
    uint16_t ac_heaterB: 1;
    uint16_t fan_f1: 1;
    uint16_t fan_f2: 1;
    uint16_t fan_f3: 1;
    uint16_t fan_f4: 1;
    uint16_t fan_r1: 1;
    uint16_t fan_r2: 1;
    uint16_t fan_r3: 1;
    uint16_t fan_r4: 1;
    uint16_t mood: 1;
}BIT_CONTROL_DBD;

typedef struct tag_dcontrol_value
{
    uint8_t  buckRGB[6];
    BIT_CONTROL_DBD bCtrl
}CONTROL_DBD;

/*

typedef struct tag_cctrlBit
{
	uint16_t sol_type: 1;
	uint16_t ctrl_d1: 1;
	uint16_t ctrl_d2: 1;
	uint16_t ctrl_b1: 1;
	uint16_t ctrl_b2: 1;
	uint16_t ctrl_htr: 1;
	uint16_t ctrl_relay: 1;
	uint16_t ctrl_sd: 1;
	uint16_t ctrl_fan: 1;	
	uint16_t x9: 1;	
	uint16_t x10: 1;	
	uint16_t x11: 1;	
	uint16_t x12: 1;	
	uint16_t x13: 1;	
	uint16_t x14: 1;			
	uint16_t x15: 1;						
}BIT_CB_CTRL;

typedef union tag_solBit_union
{
	uint16_t value;
	BIT_CB_CTRL b;
}UNION_CB_CTRL;


typedef struct tag_cbd_ctrll_cmd
{
	uint8_t cid;//1
	UNION_CB_CTRL CbCtrl;//2
	uint16_t MaxV;//2
	uint16_t MaxA;//2
}CBD_CTRL_CMD;
*/

typedef struct tag_bdcontrol_value
{
    uint8_t sof;
    uint8_t sz;
    CONTROL_DBD Ctrl_D;
    CONTROL_CBD Ctrl_C;
    uint8_t crc;
    uint8_t eof;
}BD_CONTROL;




void Drv_dbd_view(void);
void Drv_dbd_cinfo(uint8_t cid);
void Drv_dbd_TXprocess(uint8_t cid);
void Drv_dbd_controll(void);
void Drv_dbd_controll_off();
void Drv_Control_IO(uint8_t *data);

#endif