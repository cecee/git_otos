/*
 * hal_adc.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stm32f1xx_hal.h"
#include "adc.h"
#include "hal_adc.h"

#include "d_cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/
uint32_t gAdcValue[4];
extern ADC_HandleTypeDef hadc1;
extern DAC_HandleTypeDef hdac;

/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        Hal_ADC1_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/

int Hal_ADC_Init(void)
{
	Hal_Adc1_Init();
	return RET_OK;
}

/*****************************************************************************
*  Name:        Hal_Adc1_Init()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Hal_Adc1_Init(void)
{
	HAL_ADC_Start(&hadc1);
	Adc1_start();
	return RET_OK;
}

int Adc1_start(void)
{
	HAL_ADC_Start_DMA(&hadc1, gAdcValue, 3);
	return RET_OK;
}
/*
int DAC_set_voltage( uint16_t value)
{
		HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, value);
}
*/
int DAC_set_dimmer(uint16_t value)
{
		HAL_DAC_SetValue(&hdac, DAC_CHANNEL_2, DAC_ALIGN_12B_R, value);
}

void ADC_ConvTemperature(uint16_t ch0, uint16_t ch1, uint16_t cds)
{
	int16_t  tmp;
	uint32_t volt[2];
	volt[0]=(250*ch0)/4096;
	volt[1]=(250*ch1)/4096;
	tmp=volt[0]-50;
	db_up.dStatus.tmp36_0=(tmp<127)? tmp:127;
	tmp=volt[1]-50;
	db_up.dStatus.tmp36_1=(tmp<127)? tmp:127;	
	db_up.dStatus.cds=cds;
	//printf("ADC_ConvTemperature [%d] [%d] [%d]\r\n",dStatus.tmp36_0, dStatus.tmp36_1, dStatus.cds);	
}
