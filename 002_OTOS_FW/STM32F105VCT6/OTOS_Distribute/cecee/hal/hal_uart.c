/*
 * hal_uart.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "stm32f1xx_hal.h"
#include "usart.h"

#include "d_cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define HUART_TRACE(...)

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/



/*****************************************************************************
*                                Functions
/*****************************************************************************
*  Name:        Hal_Uart_SerialRxDataProcess()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/

int Hal_Uart_TxtoM(unsigned char *txdata, int txlen)
{
	HAL_StatusTypeDef state;

	if(txlen==0) return 0;
	state = HAL_UART_Transmit(&huart1, txdata, txlen, HAL_MAX_DELAY);
	if(state!=HAL_OK) return 1;
	else return 1;
}
