/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H__
#define __MAIN_H__

/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define pB1_Pin GPIO_PIN_2
#define pB1_GPIO_Port GPIOE
#define pR2_Pin GPIO_PIN_3
#define pR2_GPIO_Port GPIOE
#define pG2_Pin GPIO_PIN_4
#define pG2_GPIO_Port GPIOE
#define pB2_Pin GPIO_PIN_5
#define pB2_GPIO_Port GPIOE
#define pR3_Pin GPIO_PIN_6
#define pR3_GPIO_Port GPIOE
#define FAN_R2_AL_Pin GPIO_PIN_13
#define FAN_R2_AL_GPIO_Port GPIOC
#define FAN_R3_AL_Pin GPIO_PIN_14
#define FAN_R3_AL_GPIO_Port GPIOC
#define FAN_R4_AL_Pin GPIO_PIN_15
#define FAN_R4_AL_GPIO_Port GPIOC
#define AC_FAIL_Pin GPIO_PIN_0
#define AC_FAIL_GPIO_Port GPIOC
#define DC_FAIL_Pin GPIO_PIN_1
#define DC_FAIL_GPIO_Port GPIOC
#define BAT_FAIL_Pin GPIO_PIN_2
#define BAT_FAIL_GPIO_Port GPIOC
#define OVC_Pin GPIO_PIN_3
#define OVC_GPIO_Port GPIOC
#define FAN_F1_AL_Pin GPIO_PIN_4
#define FAN_F1_AL_GPIO_Port GPIOC
#define FAN_F2_AL_Pin GPIO_PIN_5
#define FAN_F2_AL_GPIO_Port GPIOC
#define pR6_Pin GPIO_PIN_0
#define pR6_GPIO_Port GPIOB
#define pG6_Pin GPIO_PIN_1
#define pG6_GPIO_Port GPIOB
#define pB6_Pin GPIO_PIN_2
#define pB6_GPIO_Port GPIOB
#define pG3_Pin GPIO_PIN_7
#define pG3_GPIO_Port GPIOE
#define pB3_Pin GPIO_PIN_8
#define pB3_GPIO_Port GPIOE
#define pR4_Pin GPIO_PIN_9
#define pR4_GPIO_Port GPIOE
#define pG4_Pin GPIO_PIN_10
#define pG4_GPIO_Port GPIOE
#define pB4_Pin GPIO_PIN_11
#define pB4_GPIO_Port GPIOE
#define pR5_Pin GPIO_PIN_12
#define pR5_GPIO_Port GPIOE
#define pG5_Pin GPIO_PIN_13
#define pG5_GPIO_Port GPIOE
#define pB5_Pin GPIO_PIN_14
#define pB5_GPIO_Port GPIOE
#define pMOOD_Pin GPIO_PIN_15
#define pMOOD_GPIO_Port GPIOE
#define FAN_F3_AL_Pin GPIO_PIN_10
#define FAN_F3_AL_GPIO_Port GPIOB
#define FAN_F4_AL_Pin GPIO_PIN_11
#define FAN_F4_AL_GPIO_Port GPIOB
#define pAC_HTR_B_Pin GPIO_PIN_12
#define pAC_HTR_B_GPIO_Port GPIOB
#define pAC_HTR_A_Pin GPIO_PIN_13
#define pAC_HTR_A_GPIO_Port GPIOB
#define pAC_MONITOR_Pin GPIO_PIN_14
#define pAC_MONITOR_GPIO_Port GPIOB
#define pAC_LIGHT_Pin GPIO_PIN_15
#define pAC_LIGHT_GPIO_Port GPIOB
#define pGPIO0_Pin GPIO_PIN_10
#define pGPIO0_GPIO_Port GPIOD
#define pGPIO1_Pin GPIO_PIN_11
#define pGPIO1_GPIO_Port GPIOD
#define pFAN_R4__Pin GPIO_PIN_12
#define pFAN_R4__GPIO_Port GPIOD
#define pFAN_R3__Pin GPIO_PIN_13
#define pFAN_R3__GPIO_Port GPIOD
#define pFAN_R2__Pin GPIO_PIN_14
#define pFAN_R2__GPIO_Port GPIOD
#define pFAN_R1__Pin GPIO_PIN_15
#define pFAN_R1__GPIO_Port GPIOD
#define DHT11_S1_Pin GPIO_PIN_6
#define DHT11_S1_GPIO_Port GPIOC
#define DHT11_S1_EXTI_IRQn EXTI9_5_IRQn
#define DHT11_S2_Pin GPIO_PIN_7
#define DHT11_S2_GPIO_Port GPIOC
#define DHT11_S2_EXTI_IRQn EXTI9_5_IRQn
#define DHT11_S3_Pin GPIO_PIN_8
#define DHT11_S3_GPIO_Port GPIOC
#define DHT11_S3_EXTI_IRQn EXTI9_5_IRQn
#define FAN_R1_AL_Pin GPIO_PIN_9
#define FAN_R1_AL_GPIO_Port GPIOC
#define pDOOR_OC_Pin GPIO_PIN_15
#define pDOOR_OC_GPIO_Port GPIOA
#define pSHOCK1_Pin GPIO_PIN_0
#define pSHOCK1_GPIO_Port GPIOD
#define pSHOCK2_Pin GPIO_PIN_1
#define pSHOCK2_GPIO_Port GPIOD
#define pFAN_F1__Pin GPIO_PIN_4
#define pFAN_F1__GPIO_Port GPIOD
#define pFAN_F2__Pin GPIO_PIN_5
#define pFAN_F2__GPIO_Port GPIOD
#define pFAN_F3__Pin GPIO_PIN_6
#define pFAN_F3__GPIO_Port GPIOD
#define pFAN_F4__Pin GPIO_PIN_7
#define pFAN_F4__GPIO_Port GPIOD
#define CS_FLASH_Pin GPIO_PIN_6
#define CS_FLASH_GPIO_Port GPIOB
#define pNFC_LED_Pin GPIO_PIN_7
#define pNFC_LED_GPIO_Port GPIOB
#define pSLOPE1_Pin GPIO_PIN_8
#define pSLOPE1_GPIO_Port GPIOB
#define pSLOPE2_Pin GPIO_PIN_9
#define pSLOPE2_GPIO_Port GPIOB
#define pR1_Pin GPIO_PIN_0
#define pR1_GPIO_Port GPIOE
#define pG1_Pin GPIO_PIN_1
#define pG1_GPIO_Port GPIOE

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
