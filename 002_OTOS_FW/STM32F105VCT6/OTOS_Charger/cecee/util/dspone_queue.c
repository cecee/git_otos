/*
 * dspone_queue.c
 *
 *  Created on: 2018. 01. 22.
 *      Author: Sangwon
 */

/****************************************************************************
*                               Includes
*****************************************************************************/
#include "dspone.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

/****************************************************************************
*                                Defines
*****************************************************************************/
#define MAX_QUEUE_COUNT			4
#define MAX_QUEUE_MSG_LEN		MAX_SIZE_TCP_SEND_BUF

typedef struct QueueType {
	unsigned char msg[MAX_QUEUE_MSG_LEN];
	unsigned int len;
}Queue_t;
/****************************************************************************
*                             Data types
*****************************************************************************/

/****************************************************************************
*                                Variables
*****************************************************************************/
//	static Queue_t* send_head;
//	static Queue_t* send_tail;
//	static Queue_t* rev_head;
//	static Queue_t* rev_tail;
static Queue_t gSendQueue[MAX_QUEUE_COUNT];
static Queue_t gRevQueue[MAX_QUEUE_COUNT];
static int m_sendQueueCnt;
static int m_revQueueCnt;

/****************************************************************************
*                                Functions
*****************************************************************************/
/****************************************************************************
*  Name:    Queue_Init()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
int Queue_Init(void)
{
	//DBG(MSG,"[%s(%d)]Message Queue Init\n",__FILENAME__,__LINE__);
	memset(gSendQueue,0x00,sizeof(gSendQueue));
	memset(gRevQueue,0x00,sizeof(gRevQueue));
	m_sendQueueCnt = 0;
	m_revQueueCnt = 0;

	return RET_OK;
}
/****************************************************************************
*  Name:    Queue_Send_PushBack()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
int Queue_Send_PushBack(unsigned char * msg, unsigned int len)
{
//		DBG(MSG,"[%s(%d)][queue cnt=%d](len=%d)\n",__FILENAME__,__LINE__,m_sendQueueCnt,len);

	// Check Max Queue Count
	if(m_sendQueueCnt>=MAX_QUEUE_COUNT)
	{
		DBG(WARN,"[%s(%d)]More than %d massage is pushed.\n",__FILENAME__,__LINE__,MAX_QUEUE_COUNT);
		Queue_Send_DiscardFront();
	}

	if(len>MAX_QUEUE_MSG_LEN)
	{
		DBG(ERR,"[%s(%d)] message length is too long.\n",__FILENAME__,__LINE__,len);
		return RET_ERR;
	}

	memset(gSendQueue[m_sendQueueCnt].msg,0x00,sizeof(gSendQueue[m_sendQueueCnt].msg));
	memcpy(gSendQueue[m_sendQueueCnt].msg,msg,len);
	gSendQueue[m_sendQueueCnt].len = len;

	m_sendQueueCnt++;

	return RET_OK;
}
/****************************************************************************
*  Name:    Queue_Send_PushFront()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
int Queue_Send_PushFront(unsigned char * msg, unsigned int len)
{
//		DBG(MSG,"[%s(%d)][queue cnt=%d](len=%d)%s\n",__FILENAME__,__LINE__,m_sendQueueCnt,len,msg);

	// Check Max Queue Count
	if(m_sendQueueCnt>=MAX_QUEUE_COUNT)
	{
		DBG(WARN,"[%s(%d)]More than %d message is pushed.\n",__FILENAME__,__LINE__,MAX_QUEUE_COUNT);
		Queue_Send_DiscardBack();
	}

	if(len>MAX_QUEUE_MSG_LEN)
	{
		DBG(ERR,"[%s(%d)] message length is too long.\n",__FILENAME__,__LINE__,len);
		return RET_ERR;
	}

	for(unsigned char i=(MAX_QUEUE_COUNT-1);i>0;i--)
	{
		memset(gSendQueue[i].msg,0x00,sizeof(gSendQueue[i].msg));
		memcpy(gSendQueue[i].msg,gSendQueue[i-1].msg,gSendQueue[i-1].len);
		gSendQueue[i].len = gSendQueue[i-1].len;
	}
	memset(gSendQueue[0].msg,0x00,sizeof(gSendQueue[0].msg));
	memcpy(gSendQueue[0].msg,msg,len);
	gSendQueue[0].len = len;

	m_sendQueueCnt++;

	return RET_OK;
}
/****************************************************************************
*  Name:    Queue_Send_ReadFront()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned int Queue_Send_ReadFront(unsigned char ** msg)
{
//		DBG(MSG,"[%s(%d)]\n",__FILENAME__,__LINE__);
	unsigned int nRet=0;

	// Check for empty Queue
	if(!Queue_Send_IsEmpty())
	{
		*msg = gSendQueue[0].msg;
		nRet = gSendQueue[0].len;
	}
	
	return nRet;
}
/****************************************************************************
*  Name:    Queue_Send_PopFront()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned int Queue_Send_PopFront(unsigned char * msg)
{
//		DBG(MSG,"[%s(%d)]\n",__FILENAME__,__LINE__);
	unsigned int nRet=0;

	// Check for empty Queue
	if(!Queue_Send_IsEmpty())
	{
		memcpy(msg,gSendQueue[0].msg,gSendQueue[0].len);
		nRet = gSendQueue[0].len;

		Queue_Send_DiscardFront();
	}
	
	return nRet;
}
/****************************************************************************
*  Name:    Queue_Send_DiscardFront()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Queue_Send_DiscardFront()
{
//	DBG(MSG,"[%s(%d)]\n",__FILENAME__,__LINE__);
	for(unsigned char i=0;i<MAX_QUEUE_COUNT-1;i++)
	{
		memset(gSendQueue[i].msg,0x00,sizeof(gSendQueue[i].msg));
		memcpy(gSendQueue[i].msg,gSendQueue[i+1].msg,gSendQueue[i+1].len);
		gSendQueue[i].len = gSendQueue[i+1].len;
	}
	memset(gSendQueue[MAX_QUEUE_COUNT-1].msg,0x00,sizeof(gSendQueue[MAX_QUEUE_COUNT-1].msg));
	gSendQueue[MAX_QUEUE_COUNT-1].len = 0;
	m_sendQueueCnt--;

	// Check to see if queue is empty
	if(Queue_Send_IsEmpty())
	{
		memset(gSendQueue,0x00,sizeof(gSendQueue));
		m_sendQueueCnt=0;
	}
}
/****************************************************************************
*  Name:    Queue_Send_DiscardBack()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Queue_Send_DiscardBack()
{
//	DBG(MSG,"[%s(%d)]\n",__FILENAME__,__LINE__);
	memset(gSendQueue[m_sendQueueCnt].msg,0x00,sizeof(gSendQueue[m_sendQueueCnt].msg));
	gSendQueue[m_sendQueueCnt].len = 0;
	m_sendQueueCnt--;

	// Check to see if queue is empty
	if(Queue_Send_IsEmpty())
	{
		memset(gSendQueue,0x00,sizeof(gSendQueue));
		m_sendQueueCnt=0;
	}
}
/****************************************************************************
*  Name:    Queue_Send_GetCnt()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
int Queue_Send_GetCnt(void)
{
	return m_sendQueueCnt;
}
/****************************************************************************
*  Name:    Queue_Send_IsEmpty()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned char Queue_Send_IsEmpty(void)
{
//		DBG(MSG,"[%s(%d)]m_sendQueueCnt=%d\n",__FILENAME__,__LINE__,m_sendQueueCnt);
	return m_sendQueueCnt==0?TRUE:FALSE;
}

/****************************************************************************
*  Name:    Queue_Rev_PushBack()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
int Queue_Rev_PushBack(unsigned char * msg, unsigned int len)
{
	DBG(MSG,"[%s(%d)][queue cnt=%d](len=%d)%s\n",__FILENAME__,__LINE__,m_revQueueCnt,len,msg);

	// Check Max Queue Count
	if(m_revQueueCnt>=MAX_QUEUE_COUNT)
	{
		DBG(WARN,"[%s(%d)]More than %d massage is pushed.\n",__FILENAME__,__LINE__,MAX_QUEUE_COUNT);
		Queue_Rev_DiscardFront();
	}

	if(len>MAX_QUEUE_MSG_LEN)
	{
		DBG(ERR,"[%s(%d)] message length is too long.\n",__FILENAME__,__LINE__,len);
		return RET_ERR;
	}

	memset(gRevQueue[m_revQueueCnt].msg,0x00,sizeof(gRevQueue[m_revQueueCnt].msg));
	memcpy(gRevQueue[m_revQueueCnt].msg,msg,len);
	gRevQueue[m_revQueueCnt].len = len;

	m_revQueueCnt++;

	return RET_OK;
}
/****************************************************************************
*  Name:    Queue_Rev_PopFront()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned int Queue_Rev_PopFront(unsigned char * msg)
{
	DBG(MSG,"[%s(%d)]\n",__FILENAME__,__LINE__);
	unsigned int nRet=0;

	// Check for empty Queue
	if(!Queue_Send_IsEmpty())
	{
		memcpy(msg,gRevQueue[0].msg,gRevQueue[0].len);
		nRet = gRevQueue[0].len;

		for(unsigned char i=0;i<MAX_QUEUE_COUNT-1;i++)
		{
			memset(gRevQueue[i].msg,0x00,sizeof(gRevQueue[i].msg));
			memcpy(gRevQueue[i].msg,gRevQueue[i+1].msg,gRevQueue[i+1].len);
			gRevQueue[i].len = gRevQueue[i+1].len;
		}
		memset(gRevQueue[MAX_QUEUE_COUNT-1].msg,0x00,sizeof(gRevQueue[MAX_QUEUE_COUNT-1].msg));
		gRevQueue[MAX_QUEUE_COUNT-1].len = 0;
		
		m_revQueueCnt--;
			
		// Check to see if queue is empty
		if(Queue_Send_IsEmpty())
		{
			memset(gRevQueue,0x00,sizeof(gRevQueue));
			m_revQueueCnt=0;
		}
	}
	
	return nRet;
}
/****************************************************************************
*  Name:    Queue_Rev_DiscardFront()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Queue_Rev_DiscardFront()
{
//	DBG(MSG,"[%s(%d)]\n",__FILENAME__,__LINE__);
	for(unsigned char i=0;i<MAX_QUEUE_COUNT-1;i++)
	{
		memset(gRevQueue[i].msg,0x00,sizeof(gRevQueue[i].msg));
		memcpy(gRevQueue[i].msg,gRevQueue[i+1].msg,gRevQueue[i+1].len);
		gRevQueue[i].len = gRevQueue[i+1].len;
	}
	memset(gRevQueue[MAX_QUEUE_COUNT-1].msg,0x00,sizeof(gRevQueue[MAX_QUEUE_COUNT-1].msg));
	gRevQueue[MAX_QUEUE_COUNT-1].len = 0;
	m_revQueueCnt--;
}

/****************************************************************************
*  Name:    Queue_Rev_GetCnt()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
int Queue_Rev_GetCnt(void)
{
	return m_revQueueCnt;
}
/****************************************************************************
*  Name:    Queue_Rev_IsEmpty()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned char Queue_Rev_IsEmpty(void)
{
	return m_revQueueCnt==0?TRUE:FALSE;
}

