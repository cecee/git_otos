/*
 * cli.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "cecee.h"

/****************************************************************************
 *                                Defines
*****************************************************************************/
#define Trace		DBG

#define AT_CMD_START_STR		"at+++"
#define AT_CMD_END_STR			"at---"
/****************************************************************************
 *                             Data types
*****************************************************************************/
typedef struct _tCliCmd{
	unsigned char pcnt;
	char cmd[CLI_MAX_PARAM_SIZE];
	char para1[CLI_MAX_PARAM_SIZE];
	char para2[CLI_MAX_PARAM_SIZE];
	char para3[CLI_MAX_PARAM_SIZE];
	char para4[CLI_MAX_PARAM_SIZE];
}tCliCmd;

struct cli_command{
        const char *name;                       /**< name of command */
        int min_num_p;                          /**< minimum number of parameters */
        int (* func) (int argc, char *argv[]);  /**< handler function, return non-zero for success */
        const char *help;
};

/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                           Static Variables
******************************************************************************/
//static char gCliRxBuffer[CLI_RX_BUF_SIZE];
static unsigned int gCliRxCount=0;
static unsigned char gAtCmdMode = CLI_CMD_MODE;
static tCliCmd	gClicmd;

/*****************************************************************************
*                           declare Functions
******************************************************************************/
static int Cli_cmdh_help(int argc, char *argv[]);
static int Cli_cmdh_test(int argc, char *argv[]);
static int Cli_cmdh_gps(int argc, char *argv[]);
static int Cli_cmdh_at(int argc, char *argv[]);

/*****************************************************************************
*                           declare Cmd
******************************************************************************/

/**
* CLI command handlers
*
*/
static struct cli_command cmds[] = {
	{ "help",			0, Cli_cmdh_help,	"help"},
	{ "test",			0, Cli_cmdh_test,	"test command"},
	{ "gps",			0, Cli_cmdh_gps,	"gps command"},
	{ AT_CMD_START_STR,	0, Cli_cmdh_at,		"Start AT Command Mode"},
	/* end of table */
	{ NULL,             0, NULL, NULL}
};

/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        Cli_GetCommandMode()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned char Cli_GetCommandMode(void)
{
	return gAtCmdMode;
}
/*****************************************************************************
*  Name:        Cli_SetCommandMode()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
void Cli_SetCommandMode(unsigned char setval)
{
	gAtCmdMode=setval;
	Trace(PRT,"\r\n");
	Trace(MSG,"AT Command Mode %s\n",setval==0?"off":"on");
}
/*****************************************************************************
*  Name:        At_Control_checkRxData()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned char At_Control_checkRxData(void)
{
#if 0
	unsigned int i;
	unsigned char rxdata;

	for(i=0;i<CLI_RX_BUF_SIZE;i++)
	{
		if(Hal_Uart_DebugBufferGetOneByte(&rxdata))
		{
			//Trace(PRT,"%c",rxdata);
			gCliRxBuffer[gCliRxCount++]=(char)rxdata;
		}
		else
		{
			break;
		}
	}

	if(gCliRxCount>0)
	{
		if(gCliRxBuffer[gCliRxCount-1]==0x0d)
		{
			if(gCliRxCount==1)
			{
				gCliRxCount=0;
				memset(gCliRxBuffer,0x00,CLI_RX_BUF_SIZE);
				//Trace(PRT,"Command :");
				return FALSE;
			}
			Trace(PRT,"ATCmd : %s\n",gCliRxBuffer);
			
			gCliRxCount++;
			gCliRxBuffer[gCliRxCount-1]=0x0a;
			
			return TRUE;
		}
		else
		{
			if(gCliRxCount>=CLI_RX_BUF_SIZE)
			{
				gCliRxCount=0;
				memset(gCliRxBuffer,0x00,CLI_RX_BUF_SIZE);
			}
			return FALSE;
		}
	}
#endif
	return FALSE;
}

/*****************************************************************************
*  Name:        Cli_Control_checkRxData()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned char Cli_Control_checkRxData(void)
{
	unsigned int i;
	unsigned char rxdata;
#if 0
	for(i=0;i<CLI_RX_BUF_SIZE;i++)
	{
		if(Hal_Uart_DebugBufferGetOneByte(&rxdata))
		{
			if(rxdata==0x0a || rxdata==0x0d)
			{
				Trace(PRT,"\r\n>");
				//gCliRxCount=0;
			}
			else
			{
				//Trace(PRT,"%c",rxdata);
			}
			gCliRxBuffer[gCliRxCount++]=(char)rxdata;
			//Trace(MSG,"%c",rxdata);
			//printf("%c",rxdata);
			//if(rxdata==0x0d)printf("\r");
		}
		else
		{
			break;
		}
	}

	if(gCliRxCount>0)
	{
		if(gCliRxBuffer[gCliRxCount-1]==0x0d || gCliRxBuffer[gCliRxCount-1]==0x0a)
		{
			gCliRxBuffer[gCliRxCount-1]='\0';

			if(gCliRxCount==1)
			{
				gCliRxCount=0;
				memset(gCliRxBuffer,0x00,CLI_RX_BUF_SIZE);
				//Trace(PRT,"Command :");
				return FALSE;
			}
			return TRUE;
		}
		else
		{
			if(gCliRxCount>=CLI_RX_BUF_SIZE)
			{
				gCliRxCount=0;
				memset(gCliRxBuffer,0x00,CLI_RX_BUF_SIZE);
			}
			return FALSE;
		}
	}
#endif
	return FALSE;
}
/*****************************************************************************
*  Name:        Cli_Control_checkCMD()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned char Cli_Control_checkCMD(tCliCmd* clicmd)
{
#if 0
	unsigned char i=0;
	char spc[]=" ";
	char *token;
	//char *cmd1=NULL,*cmd2=NULL,*cmd3=NULL,*cmd4=NULL,*cmd5=NULL;

	clicmd->pcnt = 0;

	if(Cli_Control_checkRxData())
	{
		//Trace(MSG,"%s",gCliRxBuffer);

		do{
			if(i==0)
			{
				token = strtok(gCliRxBuffer,spc);
			}
			else
			{
				token = strtok(NULL,spc);
			}
			if(token==NULL)break;

			if(strlen(token)<=CLI_MAX_PARAM_SIZE)
			{
				if(token!=NULL)
				{
					if(i==0)
					{
						//clicmd->cmd=Cli_Control_getCMDNum(token);
						sprintf(clicmd->cmd,"%s",token);
						//printf("cmd : %d",clicmd->cmd);
					}
					else if(i==1)
					{
						sprintf(clicmd->para1,"%s",token);
						clicmd->pcnt++;
						//printf(",para1 : %s",clicmd->para1);
					}
					else if(i==2)
					{
						sprintf(clicmd->para2,"%s",token);
						clicmd->pcnt++;
						//printf(",para2 : %s",clicmd->para2);
					}
					else if(i==3)
					{
						sprintf(clicmd->para3,"%s",token);
						clicmd->pcnt++;
						//printf(",para3 : %s",clicmd->para3);
					}
					else if(i==4)
					{
						sprintf(clicmd->para4,"%s",token);
						clicmd->pcnt++;
						//printf(",para4 : %s",clicmd->para4);
					}
					else
					{
						clicmd->pcnt++;
						Trace(MSG, "Ext : %s\n",token);
					}
				}
			}

			i++;
		}while(token !=NULL);

		gCliRxCount=0;
		memset(gCliRxBuffer,0x00,CLI_RX_BUF_SIZE);

		//printf("\r\n");

		return TRUE;
	}
#endif
	return FALSE;
}
/*****************************************************************************
*  Name:        Cli_handle_command()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int Cli_handle_command(char *cmd, int argc, char *argv[])
{
	struct cli_command *cmdh = cmds;

	while (cmdh->name && strncmp(cmdh->name, cmd, strlen(cmdh->name)))
	{
		cmdh++;
	}

	if (!cmdh->name)
	{
		Trace(ERR, "invalid command\n");
		Trace(PRT,">");

		return 1;
	}

	if (argc < cmdh->min_num_p)
	{
		Trace(ERR, "not enough parameters\n");
		return 1;
	}

	Trace(PRT,"\r\n");

	return !cmdh->func(argc, argv);
}
/*****************************************************************************
*  Name:        Cli_control_run()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
void Cli_control_run(void)
{
#if 0
	//memset(&gClicmd,0x00,sizeof(tgClicmd));
	if(Cli_GetCommandMode()==AT_CMD_MODE)			//AT Command Mode
	{
		if(At_Control_checkRxData())
		{
			if (strlen((char*)gCliRxBuffer) == 0)
			{
				return;
			}
			if(strncmp((char*)gCliRxBuffer,(char*)AT_CMD_END_STR,strlen(AT_CMD_END_STR))==0)
			{
				Cli_SetCommandMode(CLI_CMD_MODE);
				gCliRxCount=0;
				memset(gCliRxBuffer,0x00,CLI_RX_BUF_SIZE);
				return;
			}

			if(DSPOne_TxMdmUart(MDM_TX_STR, (unsigned char *)gCliRxBuffer,(unsigned short)strlen((char*)gCliRxBuffer))==RET_ERR)
			{
				Trace(ERR,"%s():DSPOne_TxMdmUart() failed....\n",__FUNCTION__);
			}
			gCliRxCount=0;
			memset(gCliRxBuffer,0x00,CLI_RX_BUF_SIZE);
		}

	}
	else if(Cli_GetCommandMode()==CLI_CMD_MODE)	//CLI Command Mode
	{
		if(Cli_Control_checkCMD(&gClicmd))
		{
			char *argv[]={gClicmd.para1, gClicmd.para2, gClicmd.para3, gClicmd.para4};

			Cli_handle_command(gClicmd.cmd, gClicmd.pcnt, argv);
		}
	}
#endif  
}
/*****************************************************************************
*  Name:        Cli_Print_Help_All()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
void Cli_Print_Help_All(void)
{
	int i=0;
	Trace(PRT,"\r\n**************************************************************\r\n");
	Trace(PRT,    "** [ CLi Command List ] **************************************\r\n");
	Trace(PRT,    "**************************************************************\r\n");
	while(cmds[i].name)
	{
		Trace(PRT,"* %6s : %s\r\n",cmds[i].name,cmds[i].help);
		i++;
	}
	Trace(PRT,"**************************************************************\r\n");
	Trace(PRT,">");
}
/*****************************************************************************
*  Name:        Cli_cmdh_at()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
static int Cli_cmdh_at(int argc, char *argv[])
{
	Cli_SetCommandMode(AT_CMD_MODE);

	return RET_OK;
}
/*****************************************************************************
*  Name:        Cli_cmdh_help()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
static int Cli_cmdh_help(int argc, char *argv[])
{
	Cli_Print_Help_All();
	return RET_OK;
}
/*****************************************************************************
*  Name:        Cli_cmdh_test()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
static int Cli_cmdh_test(int argc, char *argv[])
{
	char *param1;

	if(argv[0])param1=argv[0];
	else
	{
		return 1;
	}

	Trace(MSG,"%s()\n",__FUNCTION__);

	switch(param1[0])
	{
		case '0':
		{
			break;
		}
		case '1':
		{
			break;
		}
		case '2':
		{
			DBG(MSG,"[%s(%d)]GPS Power On\n",__FILENAME__,__LINE__);
			Drv_Gps_SetPower(SET_ON);
			break;
		}
		case '3':
		{
			DBG(MSG,"[%s(%d)]CLI Bypass\n",__FILENAME__,__LINE__);
			unsigned char pBuf[6];
			memset(pBuf,0x00,sizeof(pBuf));
			sprintf((char*)pBuf,"hello");
			int nRet=Drv_Packer_Bypass(pBuf,(unsigned char)strlen((char*)pBuf));
			if(nRet == RET_OK)
				DSPOne_SendMsgToMdm(CMD_MODEM_TASK_TCP_OPEN,0,0,0,0);
			break;
		}
		case '4':
		{
			unsigned char nState = Drv_CheckAcc();
			DBG(MSG,"[%s(%d)]Check Acc state=%d\n",__FILENAME__,__LINE__,nState);
			break;
		}
		case '5':
		{
			break;
		}
		case '6':
		{
			break;
		}
		case 'c':
		{
			DSPOne_SendMsgToMdm(CMD_MODEM_TASK_PRTC_PAK,MSG_TYPE_CONNECT,0,0,0);
			break;
		}
		default :
			break;
	}

	return 1;
}
/*****************************************************************************
*  Name:        Cli_cmdh_gps()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
static int Cli_cmdh_gps(int argc, char *argv[])
{
	char *param1;

	if(argv[0])param1=argv[0];
	else
	{
		return 1;
	}

//		Trace(MSG,"[%s(%d)]param=%c\n",__FILENAME__,__LINE__,param1[0]);

	switch(param1[0])
	{
		case 'a':
		{
			if(argv[1] == NULL)
				break;
			unsigned char num = atoi(argv[1]);
			if(num == 0)
				DSPOne_SendMsgToMain(CMD_MAIN_TASK_GPS_AUTO_ON,SET_OFF,0,0,0);
			else if(num == 1)
				DSPOne_SendMsgToMain(CMD_MAIN_TASK_GPS_AUTO_ON,SET_ON,0,0,0);
			break;
		}
		default :
			break;
	}

	return 1;
}

