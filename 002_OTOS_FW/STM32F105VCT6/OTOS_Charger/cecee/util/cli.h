/*
 * cli.h
 *
 * Created: 2018-01-09
 *  Author: SY.Shin
 */ 


#ifndef CLI_H_
#define CLI_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/
#define CLI_RX_BUF_SIZE				(MAX_DEBUG_RX_DATA_SIZE)
#define CLI_MAX_PARAM_SIZE			32

#define AT_CMD_MODE				1
#define CLI_CMD_MODE			0

/****************************************************************************
*                                Functions
*****************************************************************************/
void Hal_Uart_DebugBufferWriteData(unsigned char *str, int str_size);
void Cli_control_run(void);
void Cli_BufferInit(void);
unsigned char Cli_GetCommandMode(void);

#endif /* CLI_H_ */
