/*
 * cbuffer.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "cecee.h"

/****************************************************************************
 *                                Defines
*****************************************************************************/

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                           Static Variables
******************************************************************************/
//static unsigned char gBufDbg[MAX_DEBUG_RX_DATA_SIZE];
//static unsigned char gBufMdm[MAX_MODEM_RX_DATA_SIZE];
//static unsigned char gBufGps[MAX_GPS_RX_DATA_SIZE];

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/
#define MAX_SIZE 256 // max size of queue

int queue[MAX_SIZE+1];
int front, rear;
/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/


/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:		CircularBuffer_Init()
*  Description:
*  Arguments:
*  Returns	:
******************************************************************************/
void CircularBuffer_Init_DBG(CircularBuffer_t *cb, int size)
{
#if 0
  cb->size = size;
	cb->writePosition = 0;
	cb->readPosition = 0;
	//cb->data = (bufferData_t *)calloc(sizeof(bufferData_t), cb->size);
	cb->data = gBufDbg;
	memset(cb->data, 0x00,cb->size);
#endif
	return;
}
/*****************************************************************************
*  Name:		CircularBuffer_Init()
*  Description:
*  Arguments:
*  Returns	:
******************************************************************************/
void CircularBuffer_Init_MDM(CircularBuffer_t *cb, int size)
{
#if 0
  cb->size = size;
	cb->writePosition = 0;
	cb->readPosition = 0;
	//cb->data = (bufferData_t *)calloc(sizeof(bufferData_t), cb->size);
	cb->data = gBufMdm;
	memset(cb->data, 0x00,cb->size);
#endif
	return;
}
/*****************************************************************************
*  Name:		CircularBuffer_Init_GPS()
*  Description:
*  Arguments:
*  Returns	:
******************************************************************************/
void CircularBuffer_Init_GPS(CircularBuffer_t *cb, int size)
{
#if 0
	cb->size = size;
	cb->writePosition = 0;
	cb->readPosition = 0;
	//cb->data = (bufferData_t *)calloc(sizeof(bufferData_t), cb->size);
	cb->data = gBufGps;
	memset(cb->data, 0x00,cb->size);
#endif 
	return;
}
/*****************************************************************************
*  Name:		CircularBuffer_Init()
*  Description:
*  Arguments:
*  Returns	:
******************************************************************************/
void CircularBuffer_Init(CircularBuffer_t *cb, int size)
{
	cb->size = size;
	cb->writePosition = 0;
	cb->readPosition = 0;
	//cb->data = (bufferData_t *)calloc(sizeof(bufferData_t), cb->size);
	cb->data = (unsigned char *)malloc(cb->size);
	memset(cb->data, 0x00,cb->size);
	return;
}
/*****************************************************************************
*  Name:        CircularBuffer_free()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
void CircularBuffer_free(CircularBuffer_t *cb)
{
	if(cb->data)
		free(cb->data);

	return;
}
/*****************************************************************************
*  Name:        CircularBuffer_Full()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int CircularBuffer_Full(CircularBuffer_t *cb)
{
	return (((cb->writePosition + 1 ) % cb->size) == cb->readPosition);
}
/*****************************************************************************
*  Name:        CircularBuffer_Empty()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
int CircularBuffer_Empty(CircularBuffer_t *cb)
{
	//DBG(MSG,"[R:%d,W:%d]\n",cb->readPosition,cb->writePosition);
	return (cb->writePosition == cb->readPosition);
}
/*****************************************************************************
*  Name:        CircularBuffer_Write()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
void CircularBuffer_Write(CircularBuffer_t *cb, unsigned char data)
{
	if(cb->data==NULL)return;

	cb->data[cb->writePosition] = data;
	cb->writePosition = (cb->writePosition + 1) % cb->size;
	if(cb->writePosition == cb->readPosition)
	cb->readPosition = (cb->writePosition + 1) % cb->size;
	//DBG(MSG,"W-[R:%d,W:%d]-%02x\n",cb->readPosition,cb->writePosition,data->value);
	return;
}
/*****************************************************************************
*  Name:        CircularBuffer_Read()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
void CircularBuffer_Read(CircularBuffer_t *cb, unsigned char *data)
{
	if(cb->data==NULL)return;

	*data = cb->data[cb->readPosition];
	cb->readPosition = (cb->readPosition + 1) % cb->size;
	//DBG(MSG,"[R:%d,W:%d]\n",cb->readPosition,cb->writePosition);
	return;
}
/*****************************************************************************
*  Name:        CircularBuffer_CheckRead()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned int CircularBuffer_CheckRead(CircularBuffer_t *cb, unsigned char *buffer)
{
	unsigned int retVal=CIRCULARBUFFER_READ_FAIL;
	//Trace(MSG,"[R:%d,W:%d]\n",cb->readPosition,cb->writePosition);
	if(!CircularBuffer_Empty(cb))
	{
		//CircularBuffer_Read(&gMainCirBuffer, &data);
		*buffer = cb->data[cb->readPosition];
		cb->readPosition = (cb->readPosition + 1) % cb->size;
		retVal = CIRCULARBUFFER_READ_OK;
	}
	else
	{
		retVal =CIRCULARBUFFER_READ_FAIL;
	}
	return retVal;

}


/////////////////////////////////////////////
///Queue
////////////////////////////////////////////
void init_queue(void){
    front = rear = 0;
}

void clear_queue(void){
    front = rear;
}

int put(int k){
//	 printf("\r\n   Queue put..........k[%x] \r\n",k);
    // 큐가 꽉차있는지 확인
    if ((rear + 1) % MAX_SIZE == front){
        printf("\r\n   Queue overflow..........rear[%d] front[%d]\r\n",rear, front);
        return -1;
    }

    queue[rear] = k;
    rear = (rear+1) % MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", rear);

    return k;
}

int get(void)
{
    int i;
    if (front == rear){
   //     printf("\r\n  Queue underflow.");
        return -1;
    }
    i = queue[front];
    front = (front+1) % MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", front);

    return i;
}
//=========================================
/*
/////////////////////////////////////////////
///TX Queue
////////////////////////////////////////////
void init_Txqueue(void){
    Txfront = Txrear = 0;
}

void clear_Txqueue(void){
    Txfront = Txrear;
}

int putTx(int k){
    // 큐가 꽉차있는지 확인
    if ((Txrear + 1) % TX_MAX_SIZE == Txfront){
        printf("\r\n  Tx_Queue overflow..........Txrear[%d] Txfront[%d]\r\n",Txrear, Txfront);
        return -1;
    }

    Txqueue[Txrear] = k;
    Txrear = (Txrear+1) % TX_MAX_SIZE;
//    printf("\r\n  put==Queue rear[%d]", rear);

    return k;
}

int getTx(void)
{
    int i;
    if (Txfront == Txrear){
        return -1;
    }
    i = Txqueue[Txfront];
    Txfront = (Txfront+1) % TX_MAX_SIZE;
//    printf("\r\n  get==Queue front[%d]", front);
    return i;
}
*/
