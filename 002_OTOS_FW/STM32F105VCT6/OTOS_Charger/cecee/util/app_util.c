/*
 * app_util.c
 *
 * Created: 2018-01-09
 *  Author: SY.Shin
 */

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "math.h"

#include "cecee.h"

/****************************************************************************
 *                                Defines
*****************************************************************************/

/****************************************************************************
 *                             Data types
*****************************************************************************/


/*****************************************************************************
*                           Static Variables
******************************************************************************/

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/

/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/


/*****************************************************************************
*                                Functions
******************************************************************************/

/******************************************************************************
* Name: 	Util_FindString()
* Description:
* Arguments:
*				 line:
*					 [in]The address of the string.
*				 len:
*					 [in]The length of the string.
*				 str:
*					 [in]The specified item which you want to look for in the string.
*
* Return:
*				The function returns a pointer to the located string,
*				or a  null	pointer  if  the specified string is not found.
******************************************************************************/
char* Util_FindString(char *line, unsigned short len,char *str)
{
	unsigned short i;
	unsigned short slen;
	char *p;

	if ((NULL == line) || (NULL == str))
		return NULL;

	slen = strlen(str);
	if(slen > len)
	{
		return NULL;
	}

	p = line;
	for (i = 0;i < len - slen + 1; i++)
	{
		if (0 == strncmp (p, str, slen))
		{
			return p;
		}else{
			p++;
		}
	}
	return NULL;
}
/******************************************************************************
* Name: 	Util_FindNumberStrStr()
* Description: strStart�� strEnd ������ ���� ã��(�ִ� ���� 3�ڸ�).
* Arguments:
*				 str:
*					 [in]The address of the string.
*				 strStart:
*					 [in]Start String
*				 strEnd:
*					 [in]End String
*
* Return:  number
******************************************************************************/
int Util_FindNumberStrStr(char *str, char *strStart, char *strEnd)
{
	static char strTmp[10];
	char* p1 = NULL;
	char* p2 = NULL;

	memset(strTmp, 0x0, sizeof(strTmp));
	p1 = strstr(str, strStart);
	p2 = strstr(p1 + 1, strEnd);
	if (p1 && p2)
	{
		if((p2 - p1 - 1) < 10 )
		{
			memcpy(strTmp, p1 + 1, p2 - p1 - 1);
			return atoi(strTmp);
		}
	}
	return 0;
}
/******************************************************************************
* Name: 	Util_FindLine
* Description:
* Parameters:
*				 line:
*					 [in]The address of the string.
*				 len:
*					 [in]The length of the string.
*				 str:
*					 [in]The specified item which you want to look for in the string.
*
* Return:
*				The function returns a pointer to the located string,
*				or a  null	pointer  if  the specified string is not found.
******************************************************************************/
char* Util_FindLine(char *line, unsigned short len, char *str)
{
	unsigned short i = 0;
	unsigned short slen = 0;
	char *p = NULL;
	char *pStr = NULL;
	char *pStr2 = NULL;
	char *pStr3 = NULL;

	if ((NULL == line) || (NULL == str))
		return NULL;

	slen = strlen(str);

	pStr = malloc(slen + 4 + 1);
	if (NULL == pStr)
		 return NULL;

	if (len >= slen + 4)//two \r\n
	{
		p = line;
		memset(pStr, 0, slen + 5);
		sprintf(pStr,"\r\n%s\r\n",str);
		for (i = 0;i < len - (slen + 4) + 1; i++)
		{
			if (0 == strncmp(p, pStr, slen + 4))
			{
				free(pStr);
				return p;
			}else{
				p++;
			}
		}
	}

	if (len >= slen + 2)//two \r or two\n
	{
		p = line;

		// <CR>xx<CR>
		memset(pStr, 0, slen + 5);
		sprintf(pStr,"\r%s\r",str);

		// <LF>xx<LF>
		pStr2 = (char*)malloc(slen + 5);
		memset(pStr2, 0, slen + 5);
		sprintf(pStr2,"\n%s\n",str);

		// xx<CR><LF>
		pStr3 = (char*)malloc(slen + 5);
		memset(pStr3, 0, slen + 5);
		sprintf(pStr3,"%s\r\n",str);

		for (i = 0;i < len - (slen + 2) + 1; i++)
		{
			if ((0 == strncmp (p, pStr, slen + 2)) ||
				(0 == strncmp (p, pStr2, slen + 2)) ||
				(0 == strncmp (p, pStr3, slen + 2)))
			{
				free(pStr);
				free(pStr2);
				free(pStr3);
				pStr = NULL;
				pStr2 = NULL;
				pStr3 = NULL;
				return p;
			}else{
				p++;
			}
		}
		free(pStr2);
		free(pStr3);
		pStr2 = NULL;
		pStr3 = NULL;
	}
	free(pStr);
	pStr = NULL;

	return NULL;
}
/****************************************************************************
*  Name:        Util_Stod()
*  Description: string to double
*  Arguments:
*  Returns  :
*****************************************************************************/
double Util_Stod(const char* s)
{
	double rez = 0, sign = 1, cipher=1;

	if (*s == '-')
	{
		s++;
		sign = -1;
	}

	int nChkDot = 0;
	for (;*s;s++)
	{
//			DBG(MSG,"%c\n",*s);
		if (*s == '.')
		{
			nChkDot = 1;
		}
		else
		{
			if (*s >= '0' && *s <= '9')
				rez = rez*10 + (*s - '0');

			if (nChkDot)
				cipher=cipher*10;
		}
	}

	return (double)sign*(rez/cipher);
};
/****************************************************************************
*  Name:        Util_Trunc()
*  Description: string to float ��ȯ �Լ�
*  Arguments:
*  Returns  :
*****************************************************************************/
double Util_Trunc(double d)
{
    return (d>0) ? floor(d) : ceil(d) ;
}
/****************************************************************************
*  Name:        Util_Pow()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
int Util_Pow(int a, int b)
{
	int result=a;
	int i;
	for(i=0;i<b;i++)
		result=result*a;

	return result;
}

/****************************************************************************
*  Name:        Util_CvtEndianShort()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned short Util_CvtEndianShort(unsigned short param)
{
	unsigned char pBuf[2];
	memcpy(pBuf,(unsigned char*)&param,sizeof(unsigned short));

	unsigned short cvtShort = (pBuf[0] * 0x100) + pBuf[1];

//	DBG(MSG,"[%s(%d)]ori=0x%X,cvt=0x%X\n",__FILENAME__,__LINE__,param,cvtShort);
	return cvtShort;
}
/****************************************************************************
*  Name:        Util_CvtEndianInt()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned int Util_CvtEndianInt(unsigned int param)
{
	unsigned char pBuf[4];
	memcpy(pBuf,(unsigned char*)&param,sizeof(unsigned int));

	unsigned int cvtInt = (pBuf[0] * 0x1000000) + (pBuf[1] * 0x10000) + (pBuf[2] * 0x100) + pBuf[3];

//	DBG(MSG,"[%s(%d)]ori=0x%X,cvt=0x%X\n",__FILENAME__,__LINE__,param,cvtInt);
	return cvtInt;
}
/****************************************************************************
*  Name:        Util_Strtok()
*  Description:	���� strtok�� �Լ��� ���ӵ� �޸��� ���Ͽ� ���� ���� �� ����
*  				util_strtok()�� ��ü�Ͽ� ����
*  Arguments:	str -> truncate�� String.
*  				seps -> delimiters
*  Returns  :	If a token is found, a pointer to the beginning of the token.
*****************************************************************************/
char * Util_Strtok (char *str, char *seps)
{
    static char *tpos, *tkn, *pos = NULL;
    static char savech;

    // Specific actions for first and subsequent calls.

    if (str != NULL)// First call, set pointer.
    {
        pos = str;
        savech = 'x';
    } else // Subsequent calls, check we've done first.
    {
        if (pos == NULL)
            return NULL;

        // Then put character back and advance.
        while (*pos != '\0')
            pos++;
        *pos++ = savech;
    }

    // Detect previous end of string.

    if (savech == '\0')
        return NULL;

    // Now we have pos pointing to first character.
    // Find first separator or nul.
    tpos = pos;
    while (*tpos != '\0') {
        tkn = strchr (seps, *tpos);
        if (tkn != NULL)
            break;
        tpos++;
    }

    savech = *tpos;
    *tpos = '\0';

    return pos;
}
/****************************************************************************
*  Name:        Util_stoi()
*  Description: string to int ��ȯ �Լ�
*  Arguments:
*  Returns  :
*****************************************************************************/
int Util_Stoi(const char* s)
{
	int rez = 0, sign = 1, result=0;

	if (*s == '-')
	{
		s++;
		sign = -1;
	};

	for (;*s;s++)
	{
		int digit = *s - '0';
		if (digit >= 0 && digit <= 9)
		{
			rez = rez*10 + digit;
		}
	};

	result = sign*rez;
//	DBG(MSG,"[%s(%d)]%s,%d\n",__FUNCTION__,__LINE__,s,result);
	return result;
}
/****************************************************************************
*  Name:        Util_BinArrayCmp()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
int Util_BinArrayCmp(unsigned char* a, unsigned char* b)
{
	unsigned int lenA = strlen((char*)a);
	unsigned int lenB = strlen((char*)a);

	if(lenA != lenB)
		return RET_ERR;

	int i;
	for(i=0;i<lenA;i++)
	{
		if(a[i] != b[i])
			return RET_ERR;
	}

	return RET_OK;
}
/*****************************************************************************
*  Name:        Hex_dump()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
void Util_Hex_Dump(const char *prt,void *data, int size)
{
    /*
     * dumps size bytes of *data to stdout. Looks like: [0000] 75 6E 6B 6E
     * 6F 77 6E 20 30 FF 00 00 00 00 39 00 unknown 0.....9. (in a single
     * line of course)
     */

	unsigned char *p = data;
    unsigned char c;
    int n;
    char bytestr[4] = { 0 };
    char addrstr[10] = { 0 };
    char hexstr[16 * 3 + 5] = { 0 };
    char charstr[16 * 1 + 5] = { 0 };

    DBG(PRT,"--------------------------------------------------------------------------------\r\n");
    DBG(PRT,"------ %16s[%03d byte]-----------------------------------------------\r\n",prt,size);
    DBG(PRT,"--------------------------------------------------------------------------------\r\n");

    for (n = 1; n <= size; n++) {
        if (n % 16 == 1) {
            //store address for this line
        	snprintf(addrstr, sizeof(addrstr), "%.4x", ((unsigned int) p - (unsigned int) data));
        }

        c = *p;
        //if (isprint(c) == 0) {
        //    c = '.';
        //}
        if (c > 0x7f || c < 0x1f) {
            c = '.';
        }
        // store hex str (for left side)
        snprintf(bytestr, sizeof(bytestr), "%02X ", *p);
        strncat(hexstr, bytestr, sizeof(hexstr) - strlen(hexstr) - 1);

        // store char str (for right side)
        snprintf(bytestr, sizeof(bytestr), "%c", c);
        strncat(charstr, bytestr, sizeof(charstr) - strlen(charstr) - 1);

        if (n % 16 == 0) {
            // line completed
        	DBG(PRT,"[%4.4s]   %-50.50s  %s\r\n", addrstr, hexstr, charstr);
            hexstr[0] = 0;
            charstr[0] = 0;
        } else if (n % 8 == 0) {
            // half line: add whitespaces
        	strncat(hexstr, "  ", sizeof(hexstr) - strlen(hexstr) - 1);
        	strncat(charstr, " ", sizeof(charstr) - strlen(charstr) - 1);
        }
        p++;                    // next byte
    }

    if (strlen(hexstr) > 0) {
        // print rest of buffer if not empty
    	DBG(PRT, "[%4.4s]   %-50.50s  %s", addrstr, hexstr, charstr);
    }
    DBG(PRT,"\r\n--------------------------------------------------------------------------------\r\n");
}
