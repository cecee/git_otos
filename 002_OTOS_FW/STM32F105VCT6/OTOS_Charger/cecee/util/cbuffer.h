/*
 * cbuffer.h
 *
 */ 


#ifndef CBUFFER_H_
#define CBUFFER_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/

#define CIRCULARBUFFER_READ_OK		0x1
#define CIRCULARBUFFER_READ_FAIL	0x0

//typedef struct {
//	unsigned char value;
//} bufferData_t;

typedef struct {
	unsigned int size;			/* Buffer size */
	unsigned int readPosition;	/* Read from Circular Buffer */
	unsigned int writePosition; 	/* Read from USART Received Buffer and	Write Position in Circular buffer */
	//bufferData_t *data;
	unsigned char *data;
}CircularBuffer_t;

/****************************************************************************
*                                Functions
*****************************************************************************/
void CircularBuffer_Init(CircularBuffer_t *cb, int size);
void CircularBuffer_Init_DBG(CircularBuffer_t *cb, int size);
void CircularBuffer_Init_MDM(CircularBuffer_t *cb, int size);
void CircularBuffer_Init_GPS(CircularBuffer_t *cb, int size);

void CircularBuffer_free(CircularBuffer_t *cb);
int CircularBuffer_Full(CircularBuffer_t *cb);
int CircularBuffer_Empty(CircularBuffer_t *cb);
void CircularBuffer_Write(CircularBuffer_t *cb, unsigned char data);
void CircularBuffer_Read(CircularBuffer_t *cb, unsigned char *data);
unsigned int CircularBuffer_CheckRead(CircularBuffer_t *cb, unsigned char *buffer);

void init_queue(void);
void clear_queue(void);
int put(int k);
int get(void);

#endif /* CBUFFER_H_ */
