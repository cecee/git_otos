/*
 * dspone_queue.h
 *
 *  Created on: 2018. 01. 22.
 *      Author: Sangwon
 */

#ifndef _DSPONE_QUEUE_H_
#define _DSPONE_QUEUE_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/

/****************************************************************************
*                             Data types
*****************************************************************************/

/****************************************************************************
*                                Variables
*****************************************************************************/

/****************************************************************************
*                                Functions
*****************************************************************************/
int Queue_Init(void);
int Queue_Send_PushBack(unsigned char * msg, unsigned int len);
int Queue_Send_PushFront(unsigned char * msg, unsigned int len);
unsigned int Queue_Send_ReadFront(unsigned char ** msg);
unsigned int Queue_Send_PopFront(unsigned char * msg);
void Queue_Send_DiscardFront();
void Queue_Send_DiscardBack();
int Queue_Send_GetCnt(void);
unsigned char Queue_Send_IsEmpty(void);

int Queue_Rev_PushBack(unsigned char * msg, unsigned int len);
unsigned int Queue_Rev_PopFront(unsigned char * msg);
void Queue_Rev_DiscardFront();
int Queue_Rev_GetCnt(void);
unsigned char Queue_Rev_IsEmpty(void);

#endif /* _DSPONE_QUEUE_H_ */

