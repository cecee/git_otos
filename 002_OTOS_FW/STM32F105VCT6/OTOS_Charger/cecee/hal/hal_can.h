#ifndef HAL_CAN_H_
#define HAL_CAN_H_

int Hal_CAN_Init(void);
void Hal_CAN1_writer(uint32_t can_id, uint8_t *data);
#endif