/*
 * hal_uart.h
 *
 * Created: 2018-01-11
 *  Author: SY.Shin
 */ 


#ifndef HAL_UART_H_
#define HAL_UART_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/

/****************************************************************************
*                                Functions
*****************************************************************************/
int Hal_Uart_Init(void);
int Hal_Uart_DebugInit(void);
void Hal_Uart_DebugBufferWriteData(unsigned char *str, int str_size);
unsigned char Hal_Uart_DebugBufferGetOneByte(unsigned char *rx_data);
unsigned char Hal_Uart_DebugBufferEmty(void);



//int Hal_Uart_ModemInit(void);
//int Hal_Uart_ModemTxStr(unsigned char *str);
//int Hal_Uart_ModemTxData(unsigned char *txdata, unsigned short txlen);

unsigned short Hal_Uart_BmsRxDataProcess(unsigned char rxdata);
unsigned short Hal_Uart_SerialRxDataProcess(unsigned char rxdata);
int Hal_Uart_BmsTxData(unsigned char *txdata, unsigned short txlen);
int Hal_Uart_ConsoleTxData(unsigned char *txdata, unsigned short txlen);

/*
void Hal_Uart_ModemBufferWriteOneByte(unsigned char d1);
unsigned char Hal_Uart_ModemBufferGetOneByte(unsigned char *rx_data);
unsigned char Hal_Uart_ModemBufferEmty(void);
//void Hal_Uart_SetModemRxModeToTcp(unsigned char set_val);
void Hal_Uart_ChkInequalitySign(unsigned char set_val);
int Hal_Uart_GpsInit(void);
int Hal_Uart_GpsTxStr(unsigned char *str);
int Hal_Uart_GpsTxData(unsigned char *txdata, unsigned short txlen);
void Hal_Uart_GpsRxDataProcess(unsigned char rxdata);
unsigned char Hal_Uart_GpsBufferGetOneByte(unsigned char *rx_data);
void Hal_Uart_GpsBufferWriteOneByte(unsigned char d1);
unsigned char Hal_Uart_GpsBufferEmty(void);
*/
#endif /* HAL_UART_H_ */
