/*
 * hal_uart.c
 *
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "stdlib.h"
#include "string.h"
#include "stm32f1xx_hal.h"
#include "usart.h"

#include "cecee.h"
/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define HUART_TRACE(...)
#define HUART_TRACE(...)				DBG(__VA_ARGS__)

#define UART_U3_RX_IDLE		0
#define UART_U3_RX_DATA	1

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                           Static Variables
******************************************************************************/

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/
static CircularBuffer_t gDebugRxBuf;
uint8_t bms_buf[128];
uint8_t bmsAddBuf[16];
uint8_t console_sbuf[32];
uint8_t console_buf[32];
 uint8_t bms_data_cnt=0;

uint8_t BMS_RCV_FLAG=0;
uint8_t CONSOLE_RCV_FLAG=0;

/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/
void Hal_Uart_DebugBufferInit(void);
//void Hal_Uart_ModemBufferInit(void);
//void Hal_Uart_GpsBufferInit(void);

/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        Hal_Uart_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Hal_Uart_Init(void)
{
 	MX_USART_StartIT();
	Hal_Uart_DebugInit();
  	memset(bms_buf,0,sizeof(bms_buf));
   	memset(console_buf,0,sizeof(console_buf));
   	
   	 init_queue();
	return RET_OK;
}
/*****************************************************************************
*  Name:        Hal_Uart_DebugInit()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Hal_Uart_DebugInit(void)
{
	Hal_Uart_DebugBufferInit();

	return RET_OK;
}
/*****************************************************************************
*  Name:        Hal_Uart_DebugBufferInit()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
void Hal_Uart_DebugBufferInit(void)
{
	memset(&gDebugRxBuf, 0x00, sizeof(CircularBuffer_t));
	//CircularBuffer_Init(&gDebugRxBuf,CLI_RX_BUF_SIZE);
	//CircularBuffer_Init_DBG(&gDebugRxBuf,MAX_DEBUG_RX_DATA_SIZE);
}
/*****************************************************************************
*  Name:        Hal_Uart_DebugBufferWriteData()
*  Description:
*  Arguments:
*  Returns  :
 *****************************************************************************/
void Hal_Uart_DebugBufferWriteData(unsigned char *str, int str_size)
{
	int i=0;

	for(i=0;i<str_size;i++)
	{
		CircularBuffer_Write(&gDebugRxBuf,str[i]);

		if(Cli_GetCommandMode()==CLI_CMD_MODE)DBG(PRT,"%c",str[i]);
		if(0x0d==str[i])
		{
			break;
		}
	}
}
/*****************************************************************************
*  Name:        Hal_Uart_DebugBufferGetOneByte()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned char Hal_Uart_DebugBufferGetOneByte(unsigned char *rx_data)
{
	if(!CircularBuffer_Empty(&gDebugRxBuf))
	{
		CircularBuffer_Read(&gDebugRxBuf,rx_data);

		//*rx_data = rxdata.value;
		//printf("Rx : [%02x] | %c\n",*rx_data,((*rx_data>0x20)&&(*rx_data<0x80))?*rx_data : '-');
		//DBG(PRT,"%c",*rx_data);

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*****************************************************************************
*  Name:        Hal_Uart_DebugBufferEmty()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned char Hal_Uart_DebugBufferEmty(void)
{
	return CircularBuffer_Empty(&gDebugRxBuf);
}



/*****************************************************************************
*  Name:        Hal_Uart_BmsRxDataProcess()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned short Hal_Uart_BmsRxDataProcess(unsigned char rxdata)
{
	unsigned short ret = 0;
	static uint8_t bms_add_flag=0;
#ifdef BMS_DBUG	
  //	printf("bms_data_cnt==0 -->rxdata[%02X]\r\n",rxdata);
#endif
  if(gBms_serch_flag)
	{
		if(bms_data_cnt==0){
		//	printf("bms_data_cnt==0 -->rxdata[%02X]\r\n",rxdata);
			switch(rxdata){
				case  0x3b: //address header
						bms_add_flag=1;
						gBms_add=0xff;
						memset(bmsAddBuf,0,sizeof(bmsAddBuf));
					break;
			}
		}
		if(bms_add_flag) 
		{
			bmsAddBuf[bms_data_cnt++]=rxdata;
			if(bms_data_cnt>=4 && rxdata==0xaa){
		//		printf("[%d]bmsAddBuf[%x][%x][%x][%x][%x][%x]\r\n",bms_data_cnt, bmsAddBuf[0],bmsAddBuf[1],bmsAddBuf[2],bmsAddBuf[3],bmsAddBuf[4],bmsAddBuf[5]);
				Drv_bms_add_confirm(bmsAddBuf[2]);
				bms_data_cnt=0;		
				bms_add_flag=0;
			}
			else if(bms_data_cnt>=4 && rxdata==0x4b){
			//	printf("[%d]bmsAddBuf[%x][%x][%x][%x][%x][%x]\r\n",bms_data_cnt, bmsAddBuf[0],bmsAddBuf[1],bmsAddBuf[2],bmsAddBuf[3],bmsAddBuf[4],bmsAddBuf[5]);
				bms_data_cnt=0;
				gBms_add=bmsAddBuf[2];
		//		Drv_bms_add_confirm(gBms_add);
				bms_add_flag=0;
				gBms_serch_flag=0;
			}
		}	
	}	
	else 	put(rxdata);
	return ret;
}

/*****************************************************************************
*  Name:        Hal_Uart_SerialRxDataProcess()
*  Description:
*  Arguments:
*  Returns  :
******************************************************************************/
unsigned short Hal_Uart_SerialRxDataProcess(unsigned char rxdata)
{
	unsigned short ret = 0;
	static uint8_t console_in=0;
  	static uint8_t console_data_cnt=0;
	gConsole_serch_flag=1;
//	printf("Hal_Uart_SerialRxDataProcess[%x]\n",rxdata);
	
	if(gConsole_serch_flag)
	{
		 if((rxdata=='!') && (console_in==0))
		{
			console_in=1;
			console_data_cnt=0;
			memset(console_sbuf,0,32);
		} 
		if(console_in) 
		{
			console_sbuf[console_data_cnt++]=rxdata;
			if(console_data_cnt>=8 && rxdata=='@')
			{
    			console_in=0;
    			console_data_cnt=0;
    			memcpy(console_buf,console_sbuf,16);
				CONSOLE_RCV_FLAG=1;
				//printf("Hal_Uart_SerialRxDataProcess[%x]\r\n",rxdata);
			}

		}
		if(console_data_cnt>16){
				console_in=0;
				console_data_cnt=0;
		}
		
	}
	return ret;
}
/*****************************************************************************
*  Name:        Hal_Uart_BmsTxData()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Hal_Uart_BmsTxData(unsigned char *txdata, unsigned short txlen)
{
	return MX_USART_Bms_SendData(txdata, txlen);
}

int Hal_Uart_ConsoleTxData(unsigned char *txdata, unsigned short txlen)
{
	return MX_USART_Console_SendData(txdata, txlen);
}
