#ifndef HAL_ADC_H_
#define HAL_ADC_H_

int Hal_ADC_Init(void);
int Adc1_start(void);
int DAC_set_voltage( uint16_t value);
int DAC_set_current( uint16_t value);
#endif