#include "main.h"
#include "stm32f1xx_hal.h"
#include "drv_console.h"
#include "cecee.h"

#include <stdlib.h>

uint8_t gConsole_serch_flag=1;
uint8_t charger_ctrl_mode=1;

void Drv_console_RXprocess(uint8_t *buffer)
{
	uint8_t i;
	uint8_t c1,c2;
	uint8_t val1[3];
	uint8_t val2[3];
	uint16_t voltage, current;
	uint16_t int_val1, int_val2;
	if(buffer[8]='@'){
		
		switch(buffer[1]){
      
      case 'A':
        /*
				val1[0]=console_buf[2];
				val1[1]=console_buf[3];
				val1[2]=console_buf[4];
				val2[0]=console_buf[5];
				val2[1]=console_buf[6];
				val2[2]=console_buf[7];
				int_val1 = atoi(val1);
				int_val2 = atoi(val2);
		 		Drv_sps194_set_value(int_val1,int_val2,VC_MODE);//0xfff-->68.8V
        */
        charger_ctrl_mode=1;
				break;
			case 'V':
				val1[0]=buffer[2];
				val1[1]=buffer[3];
				val1[2]=buffer[4];
				val2[0]=buffer[5];
				val2[1]=buffer[6];
				val2[2]=buffer[7];
				int_val1 = atoi(val1);
				int_val2 = atoi(val2);
		 		Drv_sps194_set_value(int_val1,int_val2,VC_MODE);//0xfff-->68.8V
        charger_ctrl_mode=0;
				break;
				
			case 'S':
				val1[0]=buffer[2];//0
				val1[1]=buffer[3];//0
				val1[2]=buffer[4];//n
				val2[0]=buffer[5];
				val2[1]=buffer[6];
				val2[2]=buffer[7];
				c1 = (uint8_t)atoi(val1);
				c2 = (uint8_t)atoi(val2);
				//gCBUP.sol_value.out_2b.D1=1;
				//gCBUP.sol_value.out_2b.D2=1;
				//gCBUP.sol_value.out_2b.B1=1;
				//gCBUP.sol_value.out_2b.B2=1;
				//printf("!S--->C1[%d] C2[%d]\r\n", c1, c2);
				switch(c1)
				{
					case 0:
						gCBUP.sol_value.bSolCtrl.D1=c2;
						break;
					case 1:
						gCBUP.sol_value.bSolCtrl.D2=c2;
						break;
					case 2:
						
						gCBUP.sol_value.bSolCtrl.B1=c2;
						break;
					case 3:
						gCBUP.sol_value.bSolCtrl.B2=c2;
						break;
					case 4:
						gCBUP.sol_value.bSolCtrl.bHTR=c2;
						break;	
					case 5:
						gCBUP.chg_value.b.relay=c2;
						break;	
					case 6:
						gCBUP.chg_value.b.dc_sd=c2;
						break;
					case 7:
						 gCBUP.chg_value.b.ctrl_fan=c2;
						break;   
        	case 8:
            gCBUP.sol_value.bSolCtrl.sol_type=c2; //0:solenoid  1:motor
						break;       
				}
				break;		
					
		}

	//printf("Solenode[%x][%x][%x][%x]\n",gCBUP.sol_value.out_2b.B1,gCBUP.sol_value.out_2b.B2,gCBUP.sol_value.out_2b.D1,gCBUP.sol_value.out_2b.D2);

	//	for(i=0;i<16;i++){
	//		printf("console_buf[%d][%x]-\r\n",i, console_buf[i]);
	//	}
	//	 printf("voltage[%d] current[%d]-\r\n", voltage, current);
	
	}
	else return;
}

void Drv_console_TXprocess()
{
 //  CB_UPLOAD
 	uint8_t sz, sz_bms, i, offset;
 	uint8_t *consol_data;
 	sz=sizeof(gCBUP);
  sz_bms=sizeof(gCBUP.bms_value);
  
 #if 0
  printf("gCBUP --size[%d] sz_bms[%d]\r\n",sz, sz_bms);

	gCBUP.bms_value.cell_votage[0]=3900;
	gCBUP.bms_value.cell_votage[1]=3901;
	gCBUP.bms_value.cell_votage[2]=3902;
	gCBUP.bms_value.cell_votage[17]=3917;
	gCBUP.bms_value.cell_votage[18]=3918;
	gCBUP.bms_value.cell_votage[19]=3919;
 
  gCBUP.bms_value.pack_temper=0xb0b1;
  gCBUP.bms_value.pack_serial=0x62;
	gCBUP.bms_value.pack_parallel=0x63;
	gCBUP.bms_value.pack_capacity=0x64;
	gCBUP.bms_value.pack_year=0x65;
	gCBUP.bms_value.pack_month=0x66;
	gCBUP.bms_value.pack_day=0x67;
	gCBUP.bms_value.pack_nation=0x68;
  gCBUP.bms_value.pack_serialNumber=0xc0c1;
  gCBUP.bms_value.pack_userData.value[0]=0xC0;
  gCBUP.bms_value.pack_userData.value[2]=0xC2;
  
 	gCBUP.bms_value.pack_Counter.value[0]=0xA0;
  gCBUP.bms_value.pack_Counter.value[5]=0xA5;
  gCBUP.bms_value.pack_cycle=0xe0e1;

 gCBUP.bms_value.pack_cellBancing.value[0] =0xAA;
 gCBUP.bms_value.pack_cellBancing.value[3] =0xDD;
 gCBUP.bms_value.pack_err.value[0] =0xA0;
 gCBUP.bms_value.pack_err.value[3] =0xA3;

 gCBUP.chg_value.voltage_setting=0x0102;
#endif
 	
 	consol_data=malloc(sz+4);
 	memcpy(consol_data+2, &gCBUP,sz);
 	consol_data[0]='!';
 	consol_data[1]=sz;
 	consol_data[sz+2]='@';
#if 0 
  offset=90;
  for(i=0;i<24;i++){
  	printf("consol_data[%d] [%x]\r\n", i+offset, consol_data[i+offset]);
  }
#endif
 	
#if defined CONSOLE
		Hal_Uart_ConsoleTxData(consol_data, sz+3);
  	#endif
 	free(consol_data);
}

void Drv_console_SetValueFromD(uint8_t* data)
{
	uint8_t what;
  uint16_t value;
  
  what=data[0];
  value=BUILD_UINT16(data[2], data[1]);
  printf("Drv_console_SetValueFromD what[%d] value[%d]\n", what, value);
  switch(what){
    case 0: //solenoid
      Drv_solenoid_control_onoff(value);
  }
}
