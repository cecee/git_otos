/*
 * drv_etc.h
 *
 * Created: 2018-01-17
 *  Author: SY.Shin
 */ 


#ifndef DRV_ETC_H_
#define DRV_ETC_H_

/****************************************************************************
*                               Includes
*****************************************************************************/

/****************************************************************************
*                                Defines
*****************************************************************************/
typedef enum{eLed_Red=0,eLed_Green,eLed_Blue}eLedType;

/****************************************************************************
*                                Functions
*****************************************************************************/
int Drv_Etc_Init(void);
int Drv_Delay(unsigned int delay_ms);
void Drv_SystemReset(void);
int Drv_Battery_SetPower(unsigned char setval);
unsigned char Drv_Battery_GetPercent(void);
int Drv_Led_SetOnOff(eLedType setled,unsigned char setval);
int Drv_Led_SetToggle(eLedType setled);
int Drv_Gpio_SetLevel(unsigned char gpio_port, unsigned char gpio_pin, unsigned char gpio_lvl);
unsigned short Drv_GetTick();
void Drv_SetDevId(unsigned char * id, unsigned char len);
unsigned char * Drv_GetDevId();
void Drv_SetSleepMode(void);
void Drv_CheckUartState(void);
void Drv_CheckBatteryAdc(void);
void Drv_RefreshWdg(void);
unsigned char Drv_GetRemoteControllerKeyValue(void);
void Drv_InitExtIntr(void);
void Drv_SpeakerOnOff(unsigned char setval);
unsigned char Drv_CheckAcc();

#endif /* DRV_ETC_H_ */
