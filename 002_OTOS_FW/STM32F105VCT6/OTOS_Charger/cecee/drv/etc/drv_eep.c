/*
 * drv_eep.c
 *
 * Created: 2018-02-01
 *  Author: SY.Shin
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
//#include "cmsis_os.h"
#include "cecee.h"
#include "stm32f1xx_hal.h"

#if (INTERNAL_EEPROM==SUPPORT)
/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define DRVEEP_TRACE(...)
#define DRVEEP_TRACE(...)				DBG(__VA_ARGS__)

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                           Static Variables
******************************************************************************/

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/

/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/


/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        Drv_Eep_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Eep_Init(void)
{
	return RET_OK;
}
/*****************************************************************************
*  Name:        Drv_Eep_DataErase()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Eep_DataErase(unsigned int addr, unsigned short len)
{
	unsigned short cnt = 0;

	if((EEP_BASE_ADDRESS+addr+len) > EEP_BASE_ADDRESS_END)return RET_ERR;

	HAL_FLASHEx_DATAEEPROM_Unlock();

	for(cnt=0;cnt<len;cnt++)
	{
		if(HAL_FLASHEx_DATAEEPROM_Erase(FLASH_TYPEERASEDATA_BYTE,EEP_BASE_ADDRESS+addr+cnt)!=HAL_OK)return RET_ERR;
	}
	
	HAL_FLASHEx_DATAEEPROM_Lock();

	return RET_OK;
}

/*****************************************************************************
*  Name:        Drv_Eep_DataWrite()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Eep_DataWrite(unsigned int addr, unsigned char *dat, unsigned short len)
{
	unsigned short cnt = 0;
	
	if((EEP_BASE_ADDRESS+addr+len) > EEP_BASE_ADDRESS_END)return RET_ERR;

	__disable_irq();
	__disable_interrupt();
	
	Drv_Eep_DataErase(addr,len);

	HAL_FLASHEx_DATAEEPROM_Unlock();

	for(cnt=0;cnt<len;cnt++)
	{
		unsigned int d = (unsigned int)(dat[cnt]);
		if(HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEERASEDATA_BYTE,EEP_BASE_ADDRESS+addr+cnt,d)!=HAL_OK)return RET_ERR;
	}

	HAL_FLASHEx_DATAEEPROM_Lock();
	
	__enable_irq();
	__enable_interrupt();
	
	return RET_OK;
}
/*****************************************************************************
*  Name:        Drv_Eep_DataRead()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Eep_DataRead(unsigned int addr, unsigned char *dat, unsigned short len)
{
	unsigned short cnt = 0;
	unsigned int address = EEP_BASE_ADDRESS;

	if((EEP_BASE_ADDRESS+addr+len) > EEP_BASE_ADDRESS_END)return RET_ERR;

	//HAL_FLASHEx_DATAEEPROM_Lock();
	
	for(cnt=0;cnt<len;cnt++)
	{
	    address = address + addr + cnt;
	    dat[cnt] = *(__IO unsigned int*)address;
	}

	//HAL_FLASHEx_DATAEEPROM_Unlock();

	return RET_OK;
}
#endif //#if (INTERNAL_EEPROM==SUPPORT)