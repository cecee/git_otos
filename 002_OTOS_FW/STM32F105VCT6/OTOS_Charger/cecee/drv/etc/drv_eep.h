/*
 * drv_eep.h
 *
 * Created: 2018-02-01
 *  Author: SY.Shin
 */ 


#ifndef DRV_EEP_H_
#define DRV_EEP_H_

/****************************************************************************
*                               Includes
*****************************************************************************/
#include "cecee.h"

#if (INTERNAL_EEPROM==SUPPORT)
/****************************************************************************
*                                Defines
*****************************************************************************/
#define EEP_BASE_ADDRESS			0x08080000
#define EEP_BASE_ADDRESS_END		0x08081FFF
/****************************************************************************
*                                Functions
*****************************************************************************/
int Drv_Eep_Init(void);
int Drv_Eep_DataErase(unsigned int addr, unsigned short len);
int Drv_Eep_DataWrite(unsigned int addr, unsigned char *dat, unsigned short len);
int Drv_Eep_DataRead(unsigned int addr, unsigned char *dat, unsigned short len);
#endif //#if (INTERNAL_EEPROM==SUPPORT)

#endif /* DRV_EEP_H_ */
