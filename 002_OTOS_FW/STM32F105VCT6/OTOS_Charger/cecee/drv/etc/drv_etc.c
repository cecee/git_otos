/*
 * drv_etc.c
 *
 * Created: 2018-01-17
 *  Author: SY.Shin
 */ 

/****************************************************************************
 *                               Includes
*****************************************************************************/
#include "string.h"
//#include "cmsis_os.h"
#include "stm32f1xx_hal.h"
#include "main.h"
//#include "rtc.h"
#include "adc.h"
#include "gpio.h"
#include "usart.h"
//#include "iwdg.h"

#include "cecee.h"

/****************************************************************************
 *                                Defines
*****************************************************************************/
//#define DRVETC_TRACE(...)
#define DRVETC_TRACE(...)				DBG(__VA_ARGS__)

/****************************************************************************
 *                             Data types
*****************************************************************************/

/*****************************************************************************
*                           Global Variables
******************************************************************************/

/*****************************************************************************
*                           Static Variables
******************************************************************************/
//Device ID (use IMEI for device id)
static unsigned char gDeviceId[MAX_SIZE_DEV_ID];

/*****************************************************************************
*                           declare Functions(global)
******************************************************************************/

/*****************************************************************************
*                           declare Functions(local)
******************************************************************************/


/*****************************************************************************
*                                Functions
******************************************************************************/

/*****************************************************************************
*  Name:        Drv_Etc_Init()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Etc_Init(void)
{
	return RET_OK;
}
/*****************************************************************************
*  Name:        Drv_Delay()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Delay(unsigned int delay_ms)
{
//	if(osKernelRunning())osDelay(delay_ms);
//	else HAL_Delay(delay_ms);
	HAL_Delay(delay_ms);
  return RET_OK;
}
/*****************************************************************************
*  Name:        Drv_SystemReset()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
void Drv_SystemReset(void)
{
	HAL_NVIC_SystemReset();
}
/*****************************************************************************
*  Name:        Drv_Battery_SetPower()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Battery_SetPower(unsigned char setval)
{
	//DRVETC_TRACE(MSG,"%s():%s\n",__FUNCTION__,setval==0?"off":"on");
/*	
  if(setval)
	{
		HAL_GPIO_WritePin(GPIO_OUT_FLAMEOUT_OFF_GPIO_Port, GPIO_OUT_FLAMEOUT_OFF_Pin, GPIO_PIN_RESET);
		Drv_Delay(10);
		HAL_GPIO_WritePin(GPIO_OUT_PWR_OUT_GPIO_Port, GPIO_OUT_PWR_OUT_Pin, GPIO_PIN_SET);
	}
	else
	{
		HAL_GPIO_WritePin(GPIO_OUT_FLAMEOUT_OFF_GPIO_Port, GPIO_OUT_FLAMEOUT_OFF_Pin, GPIO_PIN_SET);
		Drv_Delay(10);
		HAL_GPIO_WritePin(GPIO_OUT_PWR_OUT_GPIO_Port, GPIO_OUT_PWR_OUT_Pin, GPIO_PIN_RESET);
	}
        */
	return RET_OK;
}
/*****************************************************************************
*  Name:        Drv_Battery_GetPercent()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
unsigned char Drv_Battery_GetPercent(void)
{
	return HAL_ADC_Read_Battery();
}
/*****************************************************************************
*  Name:        Drv_Led_SetOnOff()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Led_SetOnOff(eLedType setled,unsigned char setval)
{
	//DRVETC_TRACE(MSG,"%s():%d-%s\n",__FUNCTION__,setled,setval==0?"off":"on");
/*
	if(setled==eLed_Green)
	{
		if(setval)
		{
			HAL_GPIO_WritePin(GPIO_OUT_LED1_GPIO_Port, GPIO_OUT_LED1_Pin, GPIO_PIN_RESET);
		}
		else HAL_GPIO_WritePin(GPIO_OUT_LED1_GPIO_Port, GPIO_OUT_LED1_Pin, GPIO_PIN_SET);
	}
	else if(setled==eLed_Red)
	{
		if(setval)
		{
			HAL_GPIO_WritePin(GPIO_OUT_LED2_GPIO_Port, GPIO_OUT_LED2_Pin, GPIO_PIN_RESET);
		}
		else HAL_GPIO_WritePin(GPIO_OUT_LED2_GPIO_Port, GPIO_OUT_LED2_Pin, GPIO_PIN_SET);

	}
	else if(setled==eLed_Blue)
	{
		if(setval)
		{
			HAL_GPIO_WritePin(GPIO_OUT_LED3_GPIO_Port, GPIO_OUT_LED3_Pin, GPIO_PIN_RESET);
		}
		else HAL_GPIO_WritePin(GPIO_OUT_LED3_GPIO_Port, GPIO_OUT_LED3_Pin, GPIO_PIN_SET);

	}
	else return RET_ERR;	
*/
	return RET_OK;
}
/*****************************************************************************
*  Name:        Drv_Led_SetToggle()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Led_SetToggle(eLedType setled)
{
	GPIO_TypeDef* GPIOx;
	unsigned short GPIO_Pin;
/*	
	//DRVETC_TRACE(MSG,"%s():%d-%s\n",__FUNCTION__,setled,setval==0?"off":"on");

	if(setled==eLed_Green){GPIO_Pin = GPIO_OUT_LED1_Pin;GPIOx=GPIO_OUT_LED1_GPIO_Port;}
	else if(setled==eLed_Red){GPIO_Pin = GPIO_OUT_LED2_Pin;GPIOx=GPIO_OUT_LED2_GPIO_Port;}
	else if(setled==eLed_Blue){GPIO_Pin = GPIO_OUT_LED3_Pin;GPIOx=GPIO_OUT_LED3_GPIO_Port;}	
	else return RET_ERR;	
	
	HAL_GPIO_TogglePin(GPIOx, GPIO_Pin);
	
*/
	return RET_OK;
}
/*****************************************************************************
*  Name:        Drv_Gpio_SetLevel()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_Gpio_SetLevel(unsigned char gpio_port, unsigned char gpio_pin, unsigned char gpio_lvl)
{
	GPIO_TypeDef* GPIOx;
	unsigned short led_reg = (unsigned short)(0x01 << gpio_pin);

	if(gpio_port==1)GPIOx = GPIOA;
	else if(gpio_port==2)GPIOx = GPIOB;
	else if(gpio_port==3)GPIOx = GPIOC;
	
	HAL_GPIO_WritePin(GPIOx, led_reg, (gpio_lvl==SET_ON)?GPIO_PIN_SET : GPIO_PIN_RESET);
	
	return RET_OK;
}
/*****************************************************************************
*  Name:        Drv_GetTick()
*  Description:  get tick counter started from OS boot up in seconds.
*  Arguments: 
*  Returns  : 
******************************************************************************/
unsigned short Drv_GetTick()
{
	// xTaskGetTickCount() in msec
	unsigned int nTick = xTaskGetTickCount()/1000;
//		DRVETC_TRACE(MSG,"[%s(%d)]nTick=%d\n",__FILENAME__,__LINE__,nTick);
	return nTick;
}

/****************************************************************************
*  Name:        Drv_SetDevId()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Drv_SetDevId(unsigned char * id, unsigned char len)
{
	memset(gDeviceId,0x30,sizeof(gDeviceId));
	memcpy(gDeviceId,id,len);
}
/****************************************************************************
*  Name:        Drv_GetDevId()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned char * Drv_GetDevId()
{
	return gDeviceId;
}
/****************************************************************************
*  Name:        Drv_SetSleepMode()
*  Description:This function configures the system to enter Stop mode with RTC 
* 		clocked by LSE or LSI  for current consumption measurement purpose.
* 		STOP Mode with RTC clocked by LSE/LSI
*		  =====================================   
*			- RTC Clocked by LSE or LSI
*			- Regulator in LP mode
*			- HSI, HSE OFF and LSI OFF if not used as RTC Clock source
*			- No IWDG
*			- FLASH in deep power down mode
*			- Automatic Wakeup using RTC clocked by LSE/LSI (~20s)
*  Arguments:
*  Returns  :
*****************************************************************************/
void Drv_SetSleepMode(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
#if 0
	//ADC Off
	MX_ADC_DeInit();

	//Modem Off
	Drv_Gps_SetPower(SET_OFF);
	Drv_Modem_SetPower(SET_OFF);
	Drv_Modem_SetPwrKey(SET_ON);
	Drv_Modem_SetReset(SET_ON);
	Drv_Modem_SetDTR(SET_ON);

	/* Configure all GPIO as analog to reduce current consumption on non used IOs */
	/* Enable GPIOs clock */
	__GPIOC_CLK_ENABLE();
	__GPIOH_CLK_ENABLE();
	//__GPIOA_CLK_ENABLE();
	//__GPIOB_CLK_ENABLE();

	//???
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Pin = GPIO_PIN_All;  
	HAL_GPIO_Init(GPIOH, &GPIO_InitStruct);
	//HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
	//HAL_GPIO_Init(GPIOB, &GPIO_InitStruct); 
	GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Pin = GPIO_PIN_All&(~GPIO_EXTI13_KEY_LEVEL_Pin);
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct); 

	/* Disable GPIOs clock */
	__GPIOC_CLK_DISABLE();
	__GPIOH_CLK_DISABLE();
	//__GPIOA_CLK_DISABLE();
	//__GPIOB_CLK_DISABLE();

	//HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN1);
	//HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN2);
	//HAL_PWR_DisableWakeUpPin(PWR_WAKEUP_PIN3);

	//RTC WakeUp On
	//disable_RTC_WakeUp_event();
	enable_RTC_WakeUp_event();

	//HAL_GPIO_WritePin(GPIOA, GPIO_OUT_LED3_Pin|GPIO_OUT_LED2_Pin|GPIO_OUT_LED1_Pin, GPIO_PIN_SET);//OFF

	//Enter Stop Mode --------------------------------------->>>>>>>>>
	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);
#endif
	//------------------------------------------>>>>>>>>>Exit Stop Mode
	//HAL_GPIO_WritePin(GPIOA, GPIO_OUT_LED3_Pin|GPIO_OUT_LED2_Pin|GPIO_OUT_LED1_Pin, GPIO_PIN_RESET);//ON
	/* Configures system clock after wake-up from STOP: enable HSE, PLL and select 
	PLL as system clock source (HSE and PLL are disabled in STOP mode) */
	//System Init...................................
	SystemClock_Config();
	MX_GPIO_Init_part();
	MX_ADC_Init();
}
/****************************************************************************
*  Name:        Drv_CheckUartState()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Drv_CheckUartState(void)
{
	MX_USART_CheckState();
}
/****************************************************************************
*  Name:        Drv_CheckBatteryAdc()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Drv_CheckBatteryAdc(void)
{
	HAL_ADC_CheckBattery();
}
/****************************************************************************
*  Name:        Drv_RefreshWdg()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Drv_RefreshWdg(void)
{
	MX_IWDG_Refresh();
}
/****************************************************************************
*  Name:        Drv_GetRemoteControllerKeyValue()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned char Drv_GetRemoteControllerKeyValue(void)
{
	unsigned char key=0x00;
/*	
	key = (HAL_GPIO_ReadPin(GPIO_IN_RF_KEY1_GPIO_Port, GPIO_IN_RF_KEY1_Pin)==GPIO_PIN_SET)?0x00:0x01;
	key += (HAL_GPIO_ReadPin(GPIO_IN_RF_KEY2_GPIO_Port, GPIO_IN_RF_KEY2_Pin)==GPIO_PIN_SET)?0x00:0x02;
	key += (HAL_GPIO_ReadPin(GPIO_IN_RF_KEY3_GPIO_Port, GPIO_IN_RF_KEY3_Pin)==GPIO_PIN_SET)?0x00:0x04;
*/
	return key;
}
/****************************************************************************
*  Name:        Drv_InitExtIntr()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Drv_InitExtIntr(void)
{
//	MX_GPIO_Init_Intrrupt();
}
/****************************************************************************
*  Name:        Drv_SpeakerOnOff()
*  Description:
*  Arguments:
*  Returns  :
*****************************************************************************/
void Drv_SpeakerOnOff(unsigned char setval)
{
//	if(setval)HAL_GPIO_WritePin(GPIO_OUT_SPEAKER_OUT_GPIO_Port, GPIO_OUT_SPEAKER_OUT_Pin, GPIO_PIN_SET);
//	else HAL_GPIO_WritePin(GPIO_OUT_SPEAKER_OUT_GPIO_Port, GPIO_OUT_SPEAKER_OUT_Pin, GPIO_PIN_RESET);
}
/****************************************************************************
*  Name:        Drv_CheckAcc()
*  Description:	check motor bike acc on state
*  Arguments:
*  Returns  :
*****************************************************************************/
unsigned char Drv_CheckAcc()
{
//	return (HAL_GPIO_ReadPin(GPIO_EXTI13_KEY_LEVEL_GPIO_Port, GPIO_EXTI13_KEY_LEVEL_Pin)==GPIO_PIN_SET)?BIKE_OPERATION_STATE_ACC_ON:BIKE_OPERATION_STATE_ACC_OFF;
return 0;
}

