#include "stm32f1xx_hal.h"
#include "drv_sps194.h"
#include "cecee.h"

/****************************************************************************
 *                                Defines
 *****************************************************************************/
 #define BMS_SERCH_TIMEOUT 100*5  //5sec
 
/*****************************************************************************
*                           Global Variables
******************************************************************************/
STATUS_SPS status_sps;

uint8_t step_process=0;
uint16_t step_period[4];

void sps194_adjVC(void);
void mStepCntUP(void);

/*****************************************************************************
*  Name:        init_sps194()
*  Description:  
*  Arguments: 
*  Returns  : 
******************************************************************************/
int Drv_sps194_init(void)
{
	memset(&status_sps, 0, sizeof(STATUS_SPS));
	gCBUP.chg_value.b.relay=SET_OFF;//SET_ON;
	gCBUP.chg_value.b.dc_sd=SET_OFF;
	Drv_sps194_set_relay();
	return RET_OK;
}


/*****************************************************************************
*  Name:        Drv_sps194_set_value()
*  Description:  
*  Arguments:  mode 0: cv_mode  mode 1:cc_mode mode 2:vc_mode
*  Returns  : 
******************************************************************************/
int Drv_sps194_set_value(uint16_t voltage, uint16_t current, uint8_t mode)
{
	status_sps.tick=0;
	status_sps.step_voltage=gCBUP.chg_value.voltage_outer;
	status_sps.step_current=gCBUP.chg_value.current_monitor;
	switch(mode)
	{
		case CV_MODE:
			status_sps.step_voltage=gCBUP.chg_value.voltage_outer;
			status_sps.step_current=MAX_CURRENT;
			status_sps.des_voltage=voltage; 
			status_sps.des_current=MAX_CURRENT;
			break;
		case CC_MODE:
			status_sps.step_voltage=MAX_VOLTAGE;
			status_sps.step_current=gCBUP.chg_value.current_monitor;
			status_sps.des_voltage=MAX_VOLTAGE; 
			status_sps.des_current=current;
			break;
		case VC_MODE:
//			status_sps.step_voltage=gCBUP.chg_value.voltage_outer;
//			status_sps.step_current=gCBUP.chg_value.current_monitor;
			status_sps.des_voltage=voltage; 
			status_sps.des_current=current;
			break;		
	}
	return RET_OK;
}


void sps194_adjVC(void)
{

	uint16_t monitor_vlotage, des_voltage;
	 uint16_t monitor_current, des_current;
	signed int step_voltage, step_current ;
	signed int diff_voltage,diff_current;
	
	monitor_vlotage=gCBUP.chg_value.voltage_inner;
	monitor_current= gCBUP.chg_value.current_monitor;
	
	des_voltage=status_sps.des_voltage;
	des_current =status_sps.des_current;
	
	step_voltage=status_sps.step_voltage;
	step_current=status_sps.step_current;
	
	diff_voltage=des_voltage-monitor_vlotage;
	diff_current=des_current-monitor_current;

	if(diff_voltage>5){
		step_voltage=step_voltage+5;
	}
	else	if(diff_voltage<(-5)){
		step_voltage=step_voltage-5;
	}
	else {
		if(diff_voltage>0)step_voltage++;
			if(diff_voltage<0)step_voltage--;
	}
	
	if(step_voltage<0)step_voltage=0;
	else if(step_voltage>4095)step_voltage=4095;
	status_sps.step_voltage=step_voltage;			
			
	if(diff_current>5){
		step_current=step_current+5;
	}
	else	if(diff_voltage<(-5)){
		step_current=step_current-5;
	}
	else {
		if(diff_current>0)step_current++;
		if(diff_current<0)step_current--;
	}		
			
	if(step_current<0)step_current=0;
	else if(step_current>4095)step_current=4095;
	status_sps.step_current=step_current;		

  //	 	printf("des_voltage[%d] monitor_vlotage[%d] diff_voltage[%d] step_voltage[%d] \r\n",
  //	 	des_voltage, monitor_vlotage,diff_voltage,status_sps.step_voltage);

 	 status_sps.tick++;
/*
	if(status_sps.step_voltage!=status_sps.des_voltage){
		if(status_sps.step_voltage>status_sps.des_voltage) {
			status_sps.step_voltage-=STEP_VOLTAGE;
			if(status_sps.step_voltage<=status_sps.des_voltage)status_sps.step_voltage=status_sps.des_voltage;
		}
		else {
			status_sps.step_voltage+=STEP_VOLTAGE;
			if(status_sps.step_voltage>=status_sps.des_voltage)status_sps.step_voltage=status_sps.des_voltage;
		}
	}
*/
/*
	if(status_sps.step_current!=status_sps.des_current){
		if(status_sps.step_current>status_sps.des_current) {
			status_sps.step_current-=STEP_CURRENT;
			if(status_sps.step_current<=status_sps.des_current)status_sps.step_current=status_sps.des_current;
		}
		else {
			status_sps.step_current+=STEP_CURRENT;
			if(status_sps.step_current>=status_sps.des_voltage)status_sps.step_current=status_sps.des_current;
		}
	}
	*/
//	if((status_sps.step_voltage==status_sps.des_voltage)&&(status_sps.step_current==status_sps.des_current) )gCBUP.chg_value.b.relay=SET_ON;
//	DBG(MSG, "step_voltage[%d] step_current[%d] relay[%d]\n", status_sps.step_voltage, status_sps.step_current, gCBUP.chg_value.b.relay);
	//Drv_sps194_writer_dac(status_sps.step_voltage, status_sps.step_current);
	Drv_sps194_writer_dac_value(status_sps.step_voltage, status_sps.step_current);
	//Drv_sps194_writer_dac_value()
}

void mStepCntUP(void)
{
	step_period[0]++;
	if(step_period[0]>=10){
		step_period[0]=0;
		step_period[1]++;
	}
}

void mSet_ChargerModeLed(uint8_t mode)
{
  switch(mode){
    case 0://no charging
        gCBUP.chg_value.b.pre_charging=0;
        gCBUP.chg_value.b.cv_charging=0;
        gCBUP.chg_value.b.cc_charging=0;
      break;
    case 1://pre
            gCBUP.chg_value.b.pre_charging=1;
      gCBUP.chg_value.b.cv_charging=0;
      gCBUP.chg_value.b.cc_charging=0;
      break;
    case 2://cc
       gCBUP.chg_value.b.pre_charging=0;
      gCBUP.chg_value.b.cv_charging=0;
      gCBUP.chg_value.b.cc_charging=1;
      break;    
    case 3://cv
       gCBUP.chg_value.b.pre_charging=0;
      gCBUP.chg_value.b.cv_charging=1;
      gCBUP.chg_value.b.cc_charging=0;
      break; 
  }
}

uint8_t mGetBalanceStatus(uint8_t timeout_sec)
{
	uint8_t  r=0;
	mStepCntUP();
	if(step_period[1]>timeout_sec) {
		return 2;
	}
	//r=CHECK_BIT(gCBUP.bms_value.cell_bal_status[0],7);
  if(gCBUP.bms_value.pack_cellBancing.value[0] + gCBUP.bms_value.pack_cellBancing.value[1]  + gCBUP.bms_value.pack_cellBancing.value[2] + gCBUP.bms_value.pack_cellBancing.value[3]) r=1;
	return r;
}

uint8_t mPrecharging(uint8_t timeout_sec)
{
	uint8_t r;
	mStepCntUP();
	if(step_period[1]>timeout_sec) {
     gCBUP.chg_value.b.relay=0;//relay off
     Drv_sps194_set_relay();
		return 2;
	}
	if(gCBUP.bms_value.pack_voltage>450)//45V
	{
     mSet_ChargerModeLed(0);
     gCBUP.chg_value.b.relay=0;//relay off
     Drv_sps194_set_relay();
    return 0;
  
  }
  else{
  
    if(step_period[1]==30) {
    	step_period[0]=9;
      Drv_sps194_set_value(630,20,VC_MODE);
      mSet_ChargerModeLed(1);
      
      //precharge
      //setvolt:63.0V
      //setcur:2.0A
      }
    if(step_period[1]==31) {
    	//(1)RELAY_ON
      gCBUP.chg_value.b.relay=1;
      Drv_sps194_set_relay();
    }
  }
	return 1;//stay this step loop
}

uint8_t mCCcharging(uint8_t timeout_sec)
{
	mStepCntUP();	
	if((step_period[1]==0) && (step_period[0]<2) )
	{
		Drv_sps194_set_value(630,120,VC_MODE);
    	mSet_ChargerModeLed(2);
	}
	if(step_period[1]>timeout_sec) {
      mSet_ChargerModeLed(0);
     gCBUP.chg_value.b.relay=0;//relay off
     Drv_sps194_set_relay();
		return 2;
	}
  if(gCBUP.bms_value.pack_voltage>=630) {
		return 0;
	}
  return 1;
}

uint8_t mCVcharging(uint8_t timeout_sec)
{
	mStepCntUP();
	if((step_period[1]==0) && (step_period[0]<8) ){
		//Drv_sps194_set_value(630,120,VC_MODE);
   		 mSet_ChargerModeLed(3);
	}
	if(step_period[1]>timeout_sec) {
      mSet_ChargerModeLed(0);
     gCBUP.chg_value.b.relay=0;//relay off
     Drv_sps194_set_relay();
		return 2;
	}
  if(gCBUP.bms_value.pack_soc>=100) {
		return 0;
	}
  return 1;
}

void Drv_sps194_step(void)//100ms마다 들어옴
{
	uint8_t ret;
	//status_sps.process_cnt++;

	switch(step_process)
	{
		case CHG_STEP0_READY:
			//status_sps.process_cnt=0;
			//unit_step_period=0;
			if(gCBUP.chg_value.b.batt_exist && (gBms_add!=0xff)) 	{
				memset(step_period,0,sizeof(step_period));
				//put 발란싱 하고 있으면 발란싱 disable하시오...balance disable
				step_process=CHG_STEP1_BALANCE_DISABLE;
			}
			break;
		case CHG_STEP1_BALANCE_DISABLE:
			ret=mGetBalanceStatus(10);
			if(ret==2) {
				//time_out
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP0_READY;
			}	
			else if(ret==0)	{ //balance ok!!
				//put 발란싱 하고 있으면 발란싱 enable하시오...balance disable
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP2_BALANCE_ENABLE;
			}
		//	if(status_sps.process_cnt>BMS_SERCH_TIMEOUT) status_sps.process_step=CHG_STEP0_READY;
			//DBG(MSG,"process_cnt[%d]\n",status_sps.process_cnt);
			break;
	  case CHG_STEP2_BALANCE_ENABLE:	
			ret=mGetBalanceStatus(10);
			if(ret==2) {
			//time_out
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP0_READY;
			}	
			else if(ret==0)	{ //balance ok!!
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP3_PRECHARGING;
			}
	  		break;	
	  case CHG_STEP3_PRECHARGING:	
	  		ret=mPrecharging(30*60);//30분대기
	  		if(ret==2) {
			//time_out
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP0_READY;
			}	
			else if(ret==0)	{ //balance ok!!
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP4_CC_CHARGING;
			}
			break;
	 	case CHG_STEP4_CC_CHARGING:	
	 		ret=mCCcharging(120*60);//120분대기 CV구간 Time out
  	  if(ret==2) {
			//time_out
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP0_READY;
			}	
			else if(ret==0)	{ //balance ok!!
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP5_CV_CHARGING;
			}   
	 		break;
			 	
	 	case CHG_STEP5_CV_CHARGING:	
	 		ret=mCVcharging(30*60);//30분대기 CV구간 Time out
  	  		if(ret==2) {
			//time_out
				memset(step_period,0,sizeof(step_period));
				step_process=CHG_STEP0_READY;
			}	
			else if(ret==0)	{ 
				memset(step_period,0,sizeof(step_period));
		//		step_process=CHG_STEP5_CC_CHARGING;
			}   
	 		break;	  		
	  		
	}
}
/*****************************************************************************
*  Name:        Drv_sps194_process()
*  Description:  10ms마다 한번씩 들어옴
*  Arguments:  
*  Returns  : 
******************************************************************************/
void Drv_sps194_process(void)
{
	sps194_adjVC();
}


void Drv_sps194_writer_dac(uint16_t voltage, uint16_t current)
{
	uint16_t v_val, i_val;
	
	if(voltage<415) voltage=415;
		
	v_val= ((voltage*10)-4150)/85;// Vin=(Vout-41.5)/8.5 //41.5V 보다 낮은전압은???
	v_val=(4095*v_val)/ADC_FULL_SCALE;
	
	if(current<10) current=10;//current 1.0A미만 Not support
	i_val= ((current*100)+3750)/475;// Vin=(Iout+3.75)/4.75 //1A 보다 낮은전류는???	
	i_val=(4095*i_val)/ADC_FULL_SCALE;
	
	if(v_val>=4095) v_val=4095;
	if(i_val>=4095) i_val=4095;	
		
	//printf("Drv_sps194_writer_dac v_val[%d] i_val[%d] \r\n", v_val, i_val);
	DAC_set_voltage(v_val);
	DAC_set_current(i_val);
	
}

void Drv_sps194_writer_dac_value(uint16_t v_val, uint16_t i_val)
{
	DAC_set_voltage(v_val);
	DAC_set_current(i_val);
}

void Drv_sps194_off()
{
//	memset(&status_sps, 0, sizeof(STATUS_SPS));
	memset(&gCBUP, 0, sizeof(gCBUP));
}

void Drv_sps194_set_relay(void)
{
	if(gCBUP.chg_value.b.relay)
			HAL_GPIO_WritePin(GPIOD, RL_CONT_Pin,GPIO_PIN_SET); 
	else				
			HAL_GPIO_WritePin(GPIOD, RL_CONT_Pin,GPIO_PIN_RESET); 				
}

void Drv_sps194_set_DcSD(void)
{
		
	if(gCBUP.chg_value.b.dc_sd)
			HAL_GPIO_WritePin(GPIOB, DC_SD_Pin,GPIO_PIN_SET); 
	else				
			HAL_GPIO_WritePin(GPIOB, DC_SD_Pin,GPIO_PIN_RESET); 	
}


void Drv_sps194_set_fan(void)
{
		
	if(gCBUP.chg_value.b.ctrl_fan)
			HAL_GPIO_WritePin(GPIOD, FAN_CONT_Pin,GPIO_PIN_SET); 
	else				
			HAL_GPIO_WritePin(GPIOD, FAN_CONT_Pin,GPIO_PIN_RESET); 	
}

void Drv_sps194_set(void)
{
	Drv_sps194_set_relay();	
	Drv_sps194_set_DcSD();
  Drv_sps194_set_fan();
}
	