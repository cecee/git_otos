#include "main.h"
//#include "cmsis_os.h"
#include "stm32f1xx_hal.h"
#include "drv_solenoid.h"
#include "cecee.h"

//SOLENOID_VALUE status_solenoid;
uint8_t exSolPos[4]={0,};

int Drv_solenoid_init(void)
{
	memset(&gCBUP.sol_value,0,sizeof(gCBUP.sol_value));
	return RET_OK;
}

int Drv_solenoid_get_pos(void)
{
	gCBUP.sol_value.bSolPos.D1=((HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_9)*2)+HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_8))&0x03;
	gCBUP.sol_value.bSolPos.D2=((HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_11)*2)+HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_10))&0x03;
	gCBUP.sol_value.bSolPos.B1=((HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_13)*2)+HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_12))&0x03;
	gCBUP.sol_value.bSolPos.B2=((HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_15)*2)+HAL_GPIO_ReadPin(GPIOD, GPIO_PIN_14))&0x03;
	//DBG(MSG, "sol_pos [%02x] [%02x] [%02x] [%02x]\n",status_solenoid._2b.D1,status_solenoid._2b.D2,status_solenoid._2b.B1,status_solenoid._2b.B2);
	return RET_OK;
}


int Drv_solenoid_set_pos(void)
{

//SOL_D1
	uint8_t solPos[4];
	uint8_t heaterON;
	signed char cmpare;
 // printf("Drv_solenoid_set_pos===\r\n");
	
	solPos[0]=gCBUP.sol_value.bSolCtrl.D1;
	solPos[1]=gCBUP.sol_value.bSolCtrl.D2;
	solPos[2]=gCBUP.sol_value.bSolCtrl.B1;
	solPos[3]=gCBUP.sol_value.bSolCtrl.B2;
	heaterON=gCBUP.sol_value.bSolCtrl.bHTR;
	
	if(heaterON) Drv_solenoid_heater_on(1);
	else Drv_solenoid_heater_on(0);
		
	cmpare=memcmp( solPos, exSolPos, 4 );
	if(cmpare==0) return RET_OK;
	
	
		
  
  if(gCBUP.sol_value.bSolCtrl.sol_type==1)//0: solenoid 1:motor 
  {
    
    //DBG(MSG, "Drv_solenoid_set_pos [%02x][%02x][%02x][%02x]\n",solPos[0],solPos[1],solPos[2],solPos[3]);
    // 둘다 High하면 불난다.	변화가 있을때 Low상태 만들고 변환된값 적용함
    HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_RESET); 
    HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET);
     
     if(gCBUP.sol_value.bSolCtrl.D1){
            HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET); 
            HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_SET); 
     
     }
     else{
          HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_RESET); 
          HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_SET); 
     
     
     }
    
      if(gCBUP.sol_value.bSolCtrl.D2){
            HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_RESET); 
            HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_SET); 
     
     }
     else{
          HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_RESET); 
          HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_SET); 
     
     
     } 
 
      if(gCBUP.sol_value.bSolCtrl.B1){
            HAL_GPIO_WritePin(GPIOE, SOL_B1m_Pin,GPIO_PIN_RESET); 
            HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_SET); 
     
     }
     else{
          HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_RESET); 
          HAL_GPIO_WritePin(GPIOE, SOL_B1m_Pin,GPIO_PIN_SET); 
     
     
     }   
     
      if(gCBUP.sol_value.bSolCtrl.B2){
            HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_RESET); 
            HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_SET); 
      }
     else{
          HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_RESET); 
          HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_SET); 
      }       
     
  
  }
  
  else//solenoid
  {
  
      HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_B1m_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_RESET); 

      if(gCBUP.sol_value.bSolCtrl.D1)      HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_SET); 
     else      HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_RESET); 
    
     if(gCBUP.sol_value.bSolCtrl.D2)       HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_SET); 
     else      HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_RESET); 
     
      if(gCBUP.sol_value.bSolCtrl.B1)      HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_SET); 
     else      HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_RESET); 
     
     if(gCBUP.sol_value.bSolCtrl.B2)       HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_SET); 
     else      HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_RESET); 
     
  }
	
	memcpy(exSolPos,solPos,4);//keep seted position;
	return RET_OK;
}


int Drv_solenoid_heater_on(uint8_t on){
	if(on){
		HAL_GPIO_WritePin(GPIOC, nHEATER_Pin,GPIO_PIN_SET); 		
	}
	else 
		HAL_GPIO_WritePin(GPIOC, nHEATER_Pin,GPIO_PIN_RESET); 	
	return RET_OK;
}

int Drv_solenoid_control_onoff(uint16_t on)
{
    if(on){
      gCBUP.sol_value.bSolCtrl.D1=1;
      gCBUP.sol_value.bSolCtrl.D2=1;
      gCBUP.sol_value.bSolCtrl.B1=1;
      gCBUP.sol_value.bSolCtrl.B2=1;
    }
    else {
      gCBUP.sol_value.bSolCtrl.D1=0;
      gCBUP.sol_value.bSolCtrl.D2=0;
      gCBUP.sol_value.bSolCtrl.B1=0;
      gCBUP.sol_value.bSolCtrl.B2=0;  
    
    }
      HAL_GPIO_WritePin(GPIOE, SOL_D1m_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_D2m_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_B1m_Pin,GPIO_PIN_RESET); 
      HAL_GPIO_WritePin(GPIOE, SOL_B2m_Pin,GPIO_PIN_RESET); 

      if(gCBUP.sol_value.bSolCtrl.D1)      HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_SET); 
     else      HAL_GPIO_WritePin(GPIOE, SOL_D1p_Pin,GPIO_PIN_RESET); 
    
     if(gCBUP.sol_value.bSolCtrl.D2)       HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_SET); 
     else      HAL_GPIO_WritePin(GPIOE, SOL_D2p_Pin,GPIO_PIN_RESET); 
     
      if(gCBUP.sol_value.bSolCtrl.B1)      HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_SET); 
     else      HAL_GPIO_WritePin(GPIOE, SOL_B1p_Pin,GPIO_PIN_RESET); 
     
     if(gCBUP.sol_value.bSolCtrl.B2)       HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_SET); 
     else      HAL_GPIO_WritePin(GPIOE, SOL_B2p_Pin,GPIO_PIN_RESET); 

	
	return RET_OK;
}