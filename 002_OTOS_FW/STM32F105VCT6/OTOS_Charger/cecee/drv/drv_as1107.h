/*
  drv_as1107.h
*/
#ifndef DRV_AS1107_H_
#define DRV_AS1107_H_

/****************************************************************************
*                               Includes
*****************************************************************************/
#include <stdint.h>

enum {
	LED_FAIL=0,
	LED_RLY,
	LED_LD_BAL,
	LED_LOW,
	LED_BAL,
	LED_PC,
	LED_CC,
	LED_CV,
	LED_CAN,
	LED_RS485,
	LED_EXIST,
	LED_HTR,
	LED_D1,
	LED_D2,
	LED_L1,
	LED_L2,
	LED_TIME_OUT=20
};

typedef struct tag_ledBit{
	uint8_t FAIL: 1;
	uint8_t RLY: 1;
	uint8_t LD_BAT: 1;
	uint8_t LOW: 1;
	uint8_t BAL: 1;
	uint8_t PC: 1;
	uint8_t CC: 1;
	uint8_t CV: 1;
	uint8_t CAN: 1;
	uint8_t RS485: 1;
	uint8_t EXIST: 1;
	uint8_t HTR: 1;
	uint8_t D1: 1;
	uint8_t D2: 1;
	uint8_t L1: 1;
	uint8_t L2: 1;
}BIT_LED;

typedef struct tag_ui
{
	uint16_t voltage;
	uint16_t current;
	BIT_LED b;
}UI_FND;

typedef struct tag_ledTimeOut{
	uint8_t FAIL;
	uint8_t RLY;
	uint8_t LD_BAT;
	uint8_t LOW;
	uint8_t BAL;
	uint8_t PC;
	uint8_t CC;
	uint8_t CV;
	uint8_t CAN;
	uint8_t RS485;
	uint8_t EXIST;
	uint8_t HTR;
	uint8_t D1;
	uint8_t D2;
	uint8_t L1;
	uint8_t L2;
}TIME_OUT_LED;
/****************************************************************************
*                               function
*****************************************************************************/
void Drv_as1107_init(void);
void writeRegister(uint8_t addr, uint8_t value);
void writeRegisters(uint16_t value);
void displayTest(void);
void noDisplayTest(void);
void display_fnd(UI_FND ui);
void display_led(BIT_LED led);
void indicate_led(uint8_t led, uint8_t flag);
void Drv_as1107_process(void);
void display_voltage(uint16_t value);
void display_current(uint16_t value);
void Drv_as1107_StaticLED_loop(void);
#endif