#ifndef DRV_CONSOLE_H_
#define DRV_CONSOLE_H_
#include <stdint.h>


//void Drv_console_RXprocess(void);
void Drv_console_TXprocess(void);
void Drv_console_RXprocess(uint8_t *buffer);
void Drv_console_SetValueFromD(uint8_t* data);

#endif