/*
 * drv_can.h
 *
 */ 
#ifndef DRV_CAN_H_
#define DRV_CAN_H_

typedef struct tag_cctrlBit
{
	uint16_t sol_type: 1;
	uint16_t ctrl_d1: 1;
	uint16_t ctrl_d2: 1;
	uint16_t ctrl_b1: 1;
	uint16_t ctrl_b2: 1;
	uint16_t ctrl_htr: 1;
	uint16_t ctrl_relay: 1;
	uint16_t ctrl_sd: 1;
	uint16_t ctrl_fan: 1;	
	uint16_t x9: 1;	
	uint16_t x10: 1;	
	uint16_t x11: 1;	
	uint16_t x12: 1;	
	uint16_t x13: 1;	
	uint16_t x14: 1;			
	uint16_t x15: 1;						
}BIT_CB_CTRL;

typedef union tag_solBit_union
{
	uint16_t value;
	BIT_CB_CTRL b;
}UNION_CB_CTRL;


typedef struct tag_cbd_ctrll_cmd
{
	uint8_t cid;//1
	UNION_CB_CTRL CbCtrl;//2
	uint16_t MaxV;//2
	uint16_t MaxA;//2
}CBD_CTRL_CMD;

////From D board control command+++
/*
typedef struct _BIT_CONTROL_CBD
{
    uint16_t chg_relay: 1;
    uint16_t chg_dc_sd: 1;
    uint16_t chg_ctrl_fan: 1;
    uint16_t ctrl_sol_type: 1;
    uint16_t ctrl_bHTR: 1;
    uint16_t ctrl_D1: 1;
    uint16_t ctrl_D2: 1;
    uint16_t ctrl_B1: 1;
    uint16_t ctrl_B2: 1;
}BIT_CONTROL_CBD;

typedef struct _DBD_CTRL_CMD
{
    uint16_t voltage_setting;
    uint16_t current_setting;
    BIT_CONTROL_CBD bCtrl;
}DBD_CTRL_CMD;
*/
////From D board control command---

int Drv_can1_init(void);
int Drv_can1_rcv(void);
void Drv_can1_transmit(uint32_t can_id, uint8_t *data);
#endif