/*
  drv_sps194.h
*/
#ifndef DRV_SPS194_H_
#define DRV_SPS194_H_

/****************************************************************************
*                               Includes
*****************************************************************************/
#include <stdint.h>

/****************************************************************************
*                                Defines
*****************************************************************************/
#define MAX_VOLTAGE		680  //68.0V
#define MAX_CURRENT		200//20.0A
#define STEP_VOLTAGE		5 //0.5V/10mS
#define STEP_CURRENT		1 //0.1A/10mS

#define ADC_FULL_SCALE		50 //5.0V *10

enum {
	CC_MODE=0,
	CV_MODE,
	VC_MODE
};

/****************************************************************************
 *                               CHARGER STATUS STEP
*****************************************************************************/
 enum {
	CHG_STEP0_READY=0,
	CHG_STEP1_BALANCE_DISABLE,
	CHG_STEP2_BALANCE_ENABLE,
	CHG_STEP3_PRECHARGING,
	CHG_STEP4_CC_CHARGING,
	CHG_STEP5_CV_CHARGING,
	CHG_STEP6_,
	CHG_STEP7_,
	CHG_STEP8_,
	CHG_STEP9_,
	CHG_STEP10_,
	CHG_STEP11_,
	CHG_STEP12_,
	CHG_STEP13_,
	CHG_STEP14_,
	CHG_STEP15_,
	CHG_STEP16_
};

typedef struct tag_sps{
	uint32_t tick;	
	uint32_t process_cnt;
	
	uint16_t des_voltage;
	uint16_t des_current;
	uint16_t step_voltage;
	uint16_t step_current;
	uint8_t process_step;

}STATUS_SPS;

typedef struct tag_status_bit
{
	uint16_t pre_charging: 1;
	uint16_t cc_charging: 1;
	uint16_t cv_charging: 1;
	uint16_t batt_exist: 1;
	uint16_t x4: 1;
	uint16_t ac_fail: 1;
	uint16_t tmp_fail: 1;
	uint16_t fan_fail: 1;
	uint16_t dc_fail: 1;
	uint16_t load_balance: 1;	
	uint16_t relay: 1;	
	uint16_t dc_sd: 1;		
	uint16_t ctrl_fan: 1;		
	uint16_t fail_all: 1;		
	uint16_t x14: 1;		
	uint16_t x15: 1;			
}SPS_STATUS_BIT;

typedef struct tag_sps_value
{
	uint16_t voltage_setting;
	uint16_t voltage_outer;
	uint16_t voltage_inner;	
	uint16_t current_setting;
	uint16_t current_monitor;
	int16_t temperature_inner;//signed short
	int16_t temperature_outer;//signed short
	uint16_t cid;
	uint16_t cversion;
	SPS_STATUS_BIT b;
}SPS_VALUE;

/****************************************************************************
*                                Functions
*****************************************************************************/
int Drv_sps194_init(void);
void Drv_sps194_process(void);
int Drv_sps194_set_value(uint16_t voltage, uint16_t current, uint8_t mode);
void Drv_sps194_writer_dac(uint16_t voltage, uint16_t current);
void Drv_sps194_writer_dac_value(uint16_t v_val, uint16_t i_val);
void Drv_sps194_set_relay(void);
void Drv_sps194_set_DcSD(void);
void Drv_sps194_set_fan(void);
void Drv_sps194_set(void);
void Drv_sps194_step(void);
#endif