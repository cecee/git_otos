/*
  drv_solenoid.h
*/
#ifndef DRV_SOLENOID_H_
#define DRV_SOLENOID_H_

/****************************************************************************
*                               Includes
*****************************************************************************/
//#include "cecee.h"


/****************************************************************************
 *                               SOLENOID STATUS
*****************************************************************************/
typedef struct tag_solCtrlBit{
  uint8_t sol_type: 1;
	uint8_t bHTR: 1;  
	uint8_t D1: 1;
	uint8_t D2: 1;
	uint8_t B1: 1;
	uint8_t B2: 1;
}BIT_SOLENOID_CTRL;

typedef struct tag_solPosBit{
	uint8_t D1: 2;
	uint8_t D2: 2;
	uint8_t B1: 2;
	uint8_t B2: 2;
}BIT_SOLENOID_POS;

typedef struct tag_sol_status{
	BIT_SOLENOID_CTRL bSolCtrl;
	BIT_SOLENOID_POS bSolPos;
}SOLENOID_VALUE;

int Drv_solenoid_init(void);
int Drv_solenoid_get_pos(void);
int Drv_solenoid_set_pos(void);
int Drv_solenoid_heater_on(uint8_t on);
int Drv_solenoid_control_onoff(uint16_t on);

#endif